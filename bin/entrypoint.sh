#!/usr/bin/env sh
set -e

if [ -z "${POSTGRESQL_HOST}" ]; then
  echo "No database host information found; skipping db setup"
else
  if [ -z "${POSTGRESQL_PORT}" ]; then
    POSTGRESQL_PORT=5432
  fi

  while ! nc -z "$POSTGRESQL_HOST" "$POSTGRESQL_PORT"
  do
    echo "waiting for $POSTGRESQL_HOST:$POSTGRESQL_PORT"
    sleep 1
  done

  echo "Running $POSTGRESQL_DATABASE migrations"
  bundle exec sequel -m db/migrations "postgres://$POSTGRESQL_USERNAME:$POSTGRESQL_PASSWORD@$POSTGRESQL_HOST/$POSTGRESQL_DATABASE"
fi

# Then exec the container's main process
# This is what's set as CMD in a) Dockerfile b) compose c) CI
exec "$@"

