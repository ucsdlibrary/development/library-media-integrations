# Library Media Integrations

## Table of contents

- [Summary](#summary)
- [Roadmap](#roadmap)
- [Resources](#resources)
- [Built with](#built-with)
- [For Developers](#for-developers)
  - [Installation](#installation)
  - [Environment Variables](#environment-variables)
  - [Jobs](#jobs)
  - [Testing](#testing)
  - [Open Telemetry](#opentelemetry)
  - [Debugging](#debugging)
	  - [Honeycomb](#honeycomb)
	  - [K9s](#k9s)
- [Database Config](#database-config)
- [Storage Organization](#storage-organization)
  - [File Name Convention](#file-name-convention)
  - [Supported File Types](#supported-file-types)
  - [NFS Volume Mount Structure](#nfs-volume-mount-structure)
- [Contacts](#contacts)
- [Acknowledgements](#acknowledgements)

## Summary

This project provides support for streaming licensed media for all campus affiliates and replaces Digital Media Reserves (DMR). It does not integrate course/playlist information. This project performs ETL functions and scheduled licenses expiration enforement for the streaming media objects to maintain legal/copyright obligations.  It's deployed as a k8s app, with 4 cron jobs.

- Alma will be the authoriative source item/collection/expiration information.
- Primo will serve as the discovery interface for users, Kaltura/Mediaspace is secondary.
- Kaltura Media Space will provide the playback and authenticated user ACLs needed to scope to all affilaite to comply with license types.

### Process Overview
- [Process Overview for All affiliates (Phase 1)](https://docs.google.com/document/d/1dKPubCcVjReWUw5n1XC8PEkUvP4g_He27CxTK1ATPdA/edit)
- [Workflow in LucidChart](https://lucid.app/lucidchart/721aa948-d58b-4f98-a75e-79866b1ca745/edit?invitationId=inv_36b54d34-5396-483b-94f8-56d9a648fd4a&referringApp=slack&page=0_0#)

### Process Documents
- See [Define media object expiration process in Alma](https://gitlab.com/ucsdlibrary/development/library-media-integrations/-/issues/2) for more details about the functional list of steps with api spec or other information to expire an item & delete from Kalutra + local storage.
- This is already referenced here [Process Overview for All affiliates (Phase 1 - Step 4)](https://docs.google.com/document/d/1dKPubCcVjReWUw5n1XC8PEkUvP4g_He27CxTK1ATPdA/edit)

## Built with

Language, Frameworks, Tools, Versions?

<img src="images/ruby.png" alt="ruby" width="16"/>  Ruby


## For Developers

### Installation

- Clone the repository `git clone git@gitlab.com:ucsdlibrary/development/library-media-integrations.git`
- Copy `.env.sample` to `.env` and make any adjustments needed for local development (see [Environment Variables](#environment-variables))

### Docker

1. Install docker and docker-compose
2. To spin up your environment, run `bin/build`

### Environment Variables

The environment variables specified in `.env.sample` represent a mixture of safe defaults and placeholders that should
be, in some cases, updated after making a local copy (`cp .env.sample .env`).

For example, the database credentials for postgresql should not need to be updated, as they represent defaults for the
database container that will be created for development in docker compose.

The Alma API and Kaltura API information, however, contain placeholders and one needs to be careful in what values get
filled in. Some information is listed in our config repo. If you're not sure about anything else, please ask in the `#tdx-devops` Slack channel.

In particular, one _SHOULD NOT_ unset `NULL_UPLOAD=yes` and fill in the Kaltura environment values unless absolutely
necessary. There _IS NOT_ a QA/Staging environment for Kaltura, so any files uploaded to Kaltura are being sent to the
_PRODUCTION_ instance.

### Jobs

This app runs 4 cronjobs to sync our database to Alma

1. `bulk_upload` - This runs every four hours and its main purpose is to check if files from our database are ready to be uploaded to Alma.
2. `check_bulk_upload_status` - Also known as, `bulkUploadStatCheck`. This runs every 30 minutes as it checks to see if the previous job has completed, in which it then links files to Alma, publishes the record, and moves it into the completed folder in our `library-media-integrations` share drive.
3. `expire` - This runs at 0400 (4 AM) every day. This checks if any license has expired and if so, deletes it from our records, unpublishes it from Alma, and removes the streaming link attached to Alma.
4. `metadata_update_check` - Also known as `csvUpdate`. This runs every 2 hours as it scans a csv from our updates folder and updates a record. 

***CAUTION*** We are able to run these scripts directly in our terminal. Should we have any test files that can be uploaded, WILL be uploaded to kaltura and must be deleted manually. 

### Testing

Make sure the program has installed all gems by running `docker compose exec app bundle install`. Sometimes this is not needed, but included here just to be safe.

To run the full test suite, run `bin/test`. Alternatively, if that doesn't work we can write out the full command here `docker compose exec app bundle exec rspec spec`

To run a specific test, run `bin/test PATH_TO_SPEC_HERE` or `docker compose exec app bundle exec rspec PATH_TO_SPEC_HERE`

### OpenTelemetry

The docker-compose development environment adds a [jaeger][jaeger] service which acts as a local [open telemetry][otel]
collector and UI.

It will be accessible at: `http://localhost:16686/` and can be used to check trace, span, event, and attribute
information locally.

### Debugging

#### Honeycomb

In production, we can check the open telemetry information on the prod version of honeycomb for `library-media-integrations`.

1. When on the honeycomb homepage for `library-media-integrations`, click on `Expand` in the `Total Traces` box
2. Scroll down and below the `Queries` table, click on the `Traces` tab. You should now see some information on what jobs have run in the past 24 hours (default). You can change the filter to see traces from other time periods.
3. There will be an icon to the left of each trace. If you click it, you will be able to see what is running in this trace. Should there be any events, you will see them in the `span events` tab in the right side window. If they are empty, there was no work needed to be done, unless we are expecting something to happen for example, we are testing the expiration of a file and it's not showing up.

#### K9s
We can also check for logs and run the scripts in K9s.

1. Make sure you have the latest version of K9s installed and are logged into the VPN
2. Make sure you have the latest ucsd kube configs, in this case if you don't have the merged config, make sure you have the prod config. 
3. Run `k9s` and navigate to the `library-media-integrations-prod` namespace

Here you can check the logs for successful/failed cronjobs, as well as run those cronjobs should you need.

To check for cronjobs, in the `library-media-integrations` namespace type `:cronjobs` and you should see all four listed. Enter any of them to see any successful/failed jobs and enter them again to see the logs.


## Database Config
- The Library’s Kaltura/Media Integrations application requires a database to be created before deployment.
- For our implementation, the database is hosted on a separate PostgreSQL RDBMS server, and will be set up by the database administrator.
- Configuration parameters needed for deployment (and supplied by the DBA) are
  - DB host name and port number
  - DB user name
  - DB password (this will be in LastPass)
- The deployment script will create all database objects (tables, indexes, constraints, etc).

DB Schema Link

## Storage Organization

The application is going to use an Minio/S3 bucket for managing file that will be uploaded to Kaltura.

### File Name Convention

Filenames should take the form `human-readable-stuff-MMSID.ext`

Files that do not conform to this will be ignored. The `MMSID` part of the filename is critical as it is used as the
unique identifier which will be used to track the file as it is uploaded to Kaltura, for reference in pulling metadata
for the file record from Alma, etc.

### Supported File Types

The application will support processing the following types of audio/video files:

- `.mov` (video)
- `.mp4` (video)
- `.mp3` (audio)
- `.wav` (audio)

Any file types that are **NOT** part of this list will be ignored by the application and moved to the `error/` subpath as
illustrated in the [NFS Volume Mount Structure](#nfs-volume-mount-structure) section.

### NFS Volume Mount Structure

The volume name is `kalturaupload`. Files that are "ready" for upload to Kaltura will be placed at the root/top level in the
NFS volume mount. For example:

```sh
/kalturaupload/ready/file1-MMSID.mp4
/kalturaupload/ready/file2-MMSID.mp3
```

When the application attempts to process each file, it will be moved/renamed to one of the following locations/subpaths
depending on the outcome of the transaction.

`/kalturaupload/ready/completed` -- A successfully processed file that was uploaded to Kaltura

`/kalturaupload/ready/pending` -- A file that exists but did not have the required metadata available in Alma to complete the
transaction to upload to Kaltura

`/kalturaupload/ready/error` -- An unanticipated issue with processing the file. Perhaps files that did not conform to the correct file
naming convention. Files for which the Kaltura API returned an unexpected error, etc.

`/kalturaupload/ready/updates` -- A CSV containing information that a certain record needs to be updated

Example view of the bucket:

```sh
/kalturaupload/ready/
    new-file1-mmsid.ext
    new-file2-mmsid.ext
    new-file3-mmsid.ext

    /completed/
    new-file4-mmsid.ext
    new-file5-mmsid.ext

    /error/
    new-file6-mmsid.ext

    /pending/
    new-file7-mmsid.ext
    new-file8-mmsid.ext
    
	/updates/
	updates.csv
```

## Contacts
**Slack Channel**
- #tdx-kaltura-mediaspace

**Product Owner**
- Miyuki Meyer ammeyer@ucsd.edu

**CARS Licesning (License Procurement)**
- Karen Graham ammeyer@ucsd.edu
- Mayumi Anderson maa045@ucsd.edu
- Denusha Alamadas damaladas@ucsd.edu

**Campus Service Contacts**
- Canvas = Matthew Fedder matthewf@ucsd.edu
- Kaltura = Galen Davis gbdavis@ucsd.edu

## Acknowledgements/Resources

Other libraries/project used a resources or inspiration
- Alma API doc links
  - https://developers.exlibrisgroup.com/alma/apis/electronic/
  - https://developers.exlibrisgroup.com/alma/apis/bibs/
  - https://developers.exlibrisgroup.com/alma/apis/docs/bibs/R0VUIC9hbG1hd3MvdjEvYmlicy97bW1zX2lkfQ==/
- Kaltura API doc links
- Canvas API links
[jaeger]:https://www.jaegertracing.io/
[otel]:https://opentelemetry.io/

## Rerun entire pipeline
