module App
  class GenerateMrss
    BULK_UPLOAD_SERVER_BASE = "http://lib-inbox.ucsd.edu/ready/".freeze
    KALTURA_CATEGORYID_UCSD_LIBRARY_AFFILIATE_ACCESS = 329962122
    KALTURA_CONVERSION_PROFILE_ID = 25307032
    KALTURA_USER_ID = "lib-kaltura".freeze

    # @see: https://developer.kaltura.com/api-docs/XML_Schemas/Drop_Folder
    # @see: https://www.rubydoc.info/gems/kaltura-client/16.10.0/Kaltura/KalturaEntryType
    # @see: https://www.rubydoc.info/gems/kaltura-client/16.10.0/Kaltura/KalturaLicenseType
    def self.call(media_resources:)
      builder = Nokogiri::XML::Builder.new { |xml|
        xml.mrss do
          xml.channel {
            media_resources.each do |media_resource|
              xml.item {
                xml.action "add"
                xml.type Kaltura::KalturaEntryType::MEDIA_CLIP
                xml.referenceId media_resource.reference_id
                xml.userId KALTURA_USER_ID
                xml.name media_resource.name
                xml.categories {
                  xml.categoryId KALTURA_CATEGORYID_UCSD_LIBRARY_AFFILIATE_ACCESS
                }
                xml.conversionProfileId KALTURA_CONVERSION_PROFILE_ID
                # TODO: support also sending AUDIO mediaType's
                xml.media {
                  xml.mediaType Kaltura::KalturaMediaType::VIDEO
                }
                xml.contentAssets {
                  xml.content {
                    xml.urlContentResource(url: "#{BULK_UPLOAD_SERVER_BASE}#{media_resource.original_filename}")
                  }
                }
              }
            end
          }
        end
      }
      builder.to_xml
    end
  end
end
