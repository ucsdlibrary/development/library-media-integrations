require "opentelemetry/sdk"
require "fileutils"
require_relative "logger"

module App
  class Seed
    def seed_nfs
      span = OpenTelemetry::Trace.current_span
      App::Logger.logger.info("Waiting on NFS to seed with sample data")
      begin
        # Loop through all fixtures and copy to nfs
        Dir.glob("spec/fixtures/video/*.mp4") do |mp4_fixture|
          App::Logger.logger.info("Loading sample file #{mp4_fixture} to NFS...")
          FileUtils.cp(mp4_fixture, "#{ENV.fetch("MEDIA_FILES_PATH")}/")
          App::Logger.logger.info("Sample file #{mp4_fixture} successfully loaded...")
        end
      rescue => e
        App::Logger.logger.info("Failed seeding NFS with sample data")
        span.record_exception(e)
        span.status = OpenTelemetry::Trace::Status.error("Failed seeding Minio bucket with sample data: #{e.class}")
      end
      FileUtils.mkdir_p "#{ENV.fetch("MEDIA_FILES_PATH")}/#{ENV.fetch("MEDIA_COMPLETED_KEY_PREFIX")}"
      FileUtils.mkdir_p "#{ENV.fetch("MEDIA_FILES_PATH")}/#{ENV.fetch("MEDIA_PENDING_KEY_PREFIX")}"
      FileUtils.mkdir_p "#{ENV.fetch("MEDIA_FILES_PATH")}/#{ENV.fetch("MEDIA_ERRORS_KEY_PREFIX")}"
      App::Logger.logger.info("Finished seeding NFS with sample data")
    end

    def seed_csv
      span = OpenTelemetry::Trace.current_span
      App::Logger.logger.info("Waiting on NFS to seed with sample csv data")
      FileUtils.mkdir_p "#{ENV.fetch("MEDIA_FILES_PATH")}/#{ENV.fetch("MEDIA_COMPLETED_KEY_PREFIX")}"
      FileUtils.mkdir_p "#{ENV.fetch("MEDIA_FILES_PATH")}/#{ENV.fetch("MEDIA_PENDING_KEY_PREFIX")}"
      FileUtils.mkdir_p "#{ENV.fetch("MEDIA_FILES_PATH")}/#{ENV.fetch("MEDIA_ERRORS_KEY_PREFIX")}"
      FileUtils.mkdir_p "#{ENV.fetch("MEDIA_FILES_PATH")}/#{ENV.fetch("MEDIA_UPDATES_KEY_PREFIX")}"
      FileUtils.mkdir_p "#{ENV.fetch("MEDIA_FILES_PATH")}/#{ENV.fetch("MEDIA_UPDATES_COMPLETED_KEY_PREFIX")}"
      begin
        # Loop through all fixtures and copy to nfs
        Dir.glob("spec/fixtures/video/updates/*.csv") do |csv_fixture|
          App::Logger.logger.info("Loading sample CSV file #{csv_fixture} to NFS...")
          FileUtils.cp(csv_fixture, "#{ENV.fetch("MEDIA_FILES_PATH")}/#{ENV.fetch("MEDIA_UPDATES_KEY_PREFIX")}")
          App::Logger.logger.info("Sample CSV file #{csv_fixture} successfully loaded...")
        end
      rescue => e
        App::Logger.logger.info("Failed seeding NFS with sample CSV data")
        span.record_exception(e)
        span.status = OpenTelemetry::Trace::Status.error("Failed seeding Minio bucket with sample CSV data: #{e.class}")
      end
      App::Logger.logger.info("Finished seeding NFS with sample CSV data")
    end
  end
end
