require "opentelemetry/sdk"
require "fileutils"

##
# Support scanning a directory of files and determining whether they are valid for processing
module App
  class MediaFiles
    SUPPORTED_EXTENSIONS = %w[.mp4 .mov .vtt .srt].freeze
    def self.validate(filepath:, errors_prefix:)
      new.validate(filepath:, errors_prefix:)
    end

    def validate(filepath:, errors_prefix:)
      span = OpenTelemetry::Trace.current_span
      span.add_event("Initiating scan and validation", attributes: {"filepath" => filepath})
      Logger.logger.info("Initiating scan and validation for filepath #{filepath}")
      valid_files = []
      Dir["#{filepath}/*"].each do |media_file|
        next if File.directory?(media_file)
        filename = File.basename(media_file)
        if valid_file?(filename: filename)
          Logger.logger.info("#{filename} is a valid file")
          span.add_event("Found a valid file", attributes: {"filename" => filename})
          valid_files << filename
        else
          msg = "#{filename} is not a valid file"
          Logger.logger.info(msg)
          move_invalid_file(file: media_file, errors_prefix: errors_prefix)
          span.status = OpenTelemetry::Trace::Status.error(msg)
        end
      end
      valid_files
    end

    def valid_file?(filename:)
      span = OpenTelemetry::Trace.current_span
      Logger.logger.info("Checking for valid file name for: #{filename}")
      begin
        extensions_regex = SUPPORTED_EXTENSIONS.join("|")
        match_data = filename.match(/^(.*?)-([0-9]+)(?:#{extensions_regex})$/)

        if match_data
          true
        else
          false
        end
      rescue => e
        msg = "Failed to extract #{filename}: #{e.message}"
        Logger.logger.error(msg)
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        false
      end
    end

    # Move an invalid file to errors subdirectory
    def move_invalid_file(file:, errors_prefix:)
      span = OpenTelemetry::Trace.current_span
      Logger.logger.info("Moving invalid file #{file} to #{errors_prefix}")
      span.add_event("Moving file to errors subdirectory", attributes: {"filename" => file})
      FileUtils.move(file, errors_prefix)
    end
  end
end
