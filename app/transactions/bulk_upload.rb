require "dry-transaction"
require "opentelemetry/sdk"

module App
  module Transactions
    class BulkUpload
      include Dry::Transaction
      def self.call(...)
        new.call(...)
      end

      step :extract # download from s3 bucket
      step :transform # transform into application data structure
      step :persist # persist the metadata from Alma into local database
      step :generate_mrss_xml # create BulkUpload MRSS XML file
      step :initiate_bulk_upload # initiate a Kaltura Bulk Upload job
      step :persist_bulk_upload # store Kaltura Bulk Upload job details in local database

      private

      #
      # @param input [String] - Should be a file name for processing relative to MEDIA_FILES_PATH
      def extract(input)
        span = OpenTelemetry::Trace.current_span
        span.add_event("Begin extract transaction step", attributes: {"file_name" => input})
        result = Extract.call(input)
        if result.success?
          Logger.logger.info("Extract for #{input} completed")
          Success(result.value!)
        else
          Logger.logger.info("Extract for #{input} failed")
          Failure(result.failure)
        end
      rescue => e
        msg = "Failed to extract #{input}"
        Logger.logger.info(msg)
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        Logger.logger.error(msg)
        Failure(["#{msg}. #{e}"])
      end

      # @param input [String] - Should be a file name for processing relative to MEDIA_FILES_PATH
      def transform(input)
        span = OpenTelemetry::Trace.current_span
        span.add_event("Begin transform transaction step", attributes: {"file_name" => input})
        @transformer = Transform.new(file_name: input)
        @transformer.extract_bib_record
        @transformer.extract_portfolio
        if @transformer.flattened_errors.size > 0
          msg = "#{input} has validation errors #{@transformer.errors.flatten}"
          Logger.logger.error(msg)
          Failure(@transformer.flattened_errors)
        elsif @transformer.records.empty?
          msg = "No bib record for file #{input}"
          Logger.logger.error(msg)
          Failure([msg])
        elsif @transformer.portfolios.empty?
          msg = "No portfolio for file #{input}"
          Logger.logger.error(msg)
          Failure([msg])
        else
          Success(input)
        end
      rescue => e
        msg = "Failed to transform #{input}"
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        Logger.logger.error(msg)
        Failure(["#{msg}. #{e}"])
      end

      # @param input [String] - Should be a file name for processing relative to MEDIA_FILES_PATH
      def persist(input)
        span = OpenTelemetry::Trace.current_span
        span.add_event("Begin persist transaction step", attributes: {"file_name" => input})
        Logger.logger.info("Entering persist step for #{input}")
        media_resource = MediaPersister.new(records: @transformer.records, portfolios: @transformer.portfolios, file_name: input).persist
        Success(media_resource)
      rescue => e
        puts e.backtrace
        msg = "Failed to persist"
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        # media_resource.errors.full_messages
        Logger.logger.error(msg)
        Failure(["#{msg}. #{e}"])
      end

      #
      # @param media_resource [MediaResource] - Current MediaResource which should be used for generating the MRSS XML file for Kaltura upload
      # @return String - An MRSS RSS string for upload
      def generate_mrss_xml(media_resource)
        span = OpenTelemetry::Trace.current_span
        span.add_event("Begin generate_mrss_xml transaction step", attributes: {"media_resource" => media_resource.inspect})
        mrss_file = App::GenerateMrss.call(media_resources: [media_resource])
        Success(media_resource: media_resource, mrss_file: mrss_file)
      rescue => e
        msg = "Failed to generate MRSS XML file for #{media_resource.inspect}"
        Logger.logger.info(msg)
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        Logger.logger.error(msg)
        Failure(["#{msg}. #{e}"])
      end

      # @param media_resource [MediaResource] - Current instance being processed
      # @param mrss_file [String] - see App::GenerateMrss.call
      def initiate_bulk_upload(media_resource:, mrss_file:)
        span = OpenTelemetry::Trace.current_span
        span.add_event("Begin initiate_bulk_upload transaction step", attributes: {"media_resource" => media_resource.inspect, "mrss_file" => mrss_file})
        result = App::BulkUpload.call(mrss_file: mrss_file)
        Success(media_resource: media_resource, bulk_upload_result: result)
      rescue => e
        msg = "Failed to successfully initiate Kaltura Bulk Upload for #{media_resource.inspect}"
        Logger.logger.info(msg)
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        Logger.logger.error(msg)
        Failure(["#{msg}. #{e}"])
      end

      #
      # @param media_resource [MediaResource] - Current instance being processed
      # @param bulk_upload_result [Kaltura::KalturaBulkUpload] - see https://www.rubydoc.info/gems/kaltura-client/16.10.0/Kaltura/KalturaBulkUpload
      def persist_bulk_upload(media_resource:, bulk_upload_result:)
        span = OpenTelemetry::Trace.current_span
        span.add_event("Begin persist_bulk_upload transaction step", attributes: {"media_resource" => media_resource.inspect, "bulk_upload_result" => bulk_upload_result.inspect})
        ::BulkUpload.create(
          bulk_job_id: bulk_upload_result.id,
          status: :in_progress,
          media_resource: media_resource
        )
        Success(media_resource)
      rescue => e
        msg = "Failed to persist Kaltura Bulk Upload details into local database for #{media_resource.inspect}"
        Logger.logger.info(msg)
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        Logger.logger.error(msg)
        Failure(["#{msg}. #{e}"])
      end
    end
  end
end
