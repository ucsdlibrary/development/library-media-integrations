require "dry-transaction"
require "opentelemetry/sdk"

module App
  module Transactions
    class MetadataUpdateCheck
      include Dry::Transaction
      def self.call(...)
        new.call(...)
      end

      step :validate # check if mmsid is valid & exists in our database
      step :extract # pull down metadata from Alma
      step :update # persist any updates for the start_date, end_date, and license metadata from Alma into database

      private

      #
      # @param input [String] - Should be a mmsid for processing
      def validate(input)
        span = OpenTelemetry::Trace.current_span
        span.add_event("Begin validate transaction step", attributes: {"mmsid" => input})
        media_resource = MediaResource.where(reference_id: input).first
        if media_resource
          Success(media_resource)
        else
          msg = "Invalid mmsid #{input}"
          span.status = OpenTelemetry::Trace::Status.error(msg)
          Logger.logger.error(msg)
          Failure({media_resource: media_resource, message: [msg]})
        end
      rescue => e
        msg = "Failed to validate mmsid: #{input}"
        Logger.logger.info(msg)
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        Logger.logger.error(msg)
        Failure({media_resource: media_resource, message: ["#{msg}\n\n#{e}"]})
      end

      #
      # @param input [MediaResource] - instance of MediaResource object for processing
      def extract(media_resource)
        span = OpenTelemetry::Trace.current_span
        span.add_event("Begin extract metadata transaction step", attributes: {"media_resource" => media_resource.inspect})
        @transformer = Transform.new(file_name: media_resource.original_filename)
        @transformer.extract_bib_record
        @transformer.extract_portfolio
        if @transformer.flattened_errors.size > 0
          msg = "Failed to extract metadata for mmsid #{media_resource.reference_id} with errors #{@transformer.errors.flatten}"
          Logger.logger.error(msg)
          Failure({media_resource: media_resource, message: ["#{msg}\n\n#{@transformer.flattened_errors}"]})
        else
          Success(media_resource)
        end
      rescue => e
        msg = "Failed to extract metadata for mmsid: #{media_resource.reference_id}"
        Logger.logger.info(msg)
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        Logger.logger.error(msg)
        Failure({media_resource: media_resource, message: ["#{msg}\n\n#{e}"]})
      end

      #
      # @param input [MediaResource] - instance of MediaResource object for processing
      def update(media_resource)
        span = OpenTelemetry::Trace.current_span
        span.add_event("Begin update metadata transaction step", attributes: {"portfolios" => @transformer.portfolios.first.inspect})
        media_resource.start_date = @transformer.portfolios.first.available_from_date
        media_resource.end_date = @transformer.portfolios.first.available_until_date
        media_resource.license_type = @transformer.portfolios.first.access_type
        media_resource = media_resource.save
        Success(media_resource)
      rescue => e
        msg = "Failed to update metadata for mmsid: #{media_resource.reference_id}"
        Logger.logger.info(msg)
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        Logger.logger.error(msg)
        Failure({media_resource: media_resource, message: ["#{msg}\n\n#{e}"]})
      end
    end
  end
end
