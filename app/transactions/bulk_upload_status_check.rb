require "dry-transaction"
require "opentelemetry/sdk"

module App
  module Transactions
    class BulkUploadStatusCheck
      include Dry::Transaction
      def self.call(...)
        new.call(...)
      end

      step :check_bulk_upload_job_status # check to see if job has completed
      step :link # link Kaltura media file to Alma/Primo
      step :unsuppress_publish # publish record for Alma/Primo
      step :move # move the media file to completed directory if successfull

      private

      #
      # @param bulk_upload [BulkUpload] - instance of BulkUpload that is still marked as :in_progress in our local database
      def check_bulk_upload_job_status(bulk_upload)
        span = OpenTelemetry::Trace.current_span
        span.add_event("Begin check_bulk_upload_job_status transaction step", attributes: {"bulk_upload" => bulk_upload.inspect})

        job = KalturaUtils::BulkUploadJob.current_status(bulk_upload: bulk_upload)
        if KalturaUtils::BulkUploadJob.succeeded?(status: job.status)
          bulk_upload.complete_and_save
          entry_id = KalturaUtils::BulkUploadJob.bulk_upload_entry_id(bulk_upload: bulk_upload)
          KalturaUtils::PersistEntryId.call(media_record: bulk_upload.media_resource, entry_id: entry_id)
          Success(bulk_upload)
        elsif KalturaUtils::BulkUploadJob.failed?(status: job.status)
          bulk_upload.complete_and_save
          msg = "Kaltura BulkUpload job for #{bulk_upload.media_resource.original_filename} failed"
          span.status = OpenTelemetry::Trace::Status.error(msg)
          Logger.logger.error(msg)
          Failure({send_notification: true, media_resource: bulk_upload.media_resource, message: ["#{msg}. #{job.error}"]})
        else
          msg = "Kaltura BulkUpload job for #{bulk_upload.media_resource.original_filename} is still processing"
          Logger.logger.info(msg)
          Failure({send_notification: false, message: msg})
        end
      rescue => e
        msg = "Failure checking Kaltura BulkUploadJob status #{e}"
        Logger.logger.info(msg)
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        Logger.logger.error(msg)
        Failure(["#{msg}. #{e}"])
      end

      #
      # @param bulk_upload [BulkUpload] - instance of BulkUpload that is still marked as :in_progress in our local database
      def link(bulk_upload)
        span = OpenTelemetry::Trace.current_span
        span.add_event("Begin link transaction step", attributes: {"bulk_upload" => bulk_upload.inspect})
        media_resource = bulk_upload.media_resource
        KalturaUtils::CheckFile.check_kaltura_file(media_resource.entry_id)
        Logger.logger.info("Found media file, #{media_resource.name} on kaltura, linking access URL")
        @transformer = Transform.new(file_name: media_resource.original_filename)
        @transformer.extract_bib_record
        @transformer.extract_portfolio
        linker = App::Link.new(media_record: media_resource, portfolios: @transformer.portfolios)
        linker.link
        if linker.errors.flatten.size > 0
          msg = "Failed to update URL for #{media_resource.inspect}"
          Logger.logger.error(msg)
          Failure({media_resource: media_resource, message: linker.errors.flatten})
        else
          Success(bulk_upload)
        end
      rescue => e
        msg = "Failed to update link Kaltura MediaResource to Alma/Primo"
        Logger.logger.info(msg)
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        Logger.logger.error(msg)
        Failure({media_resource: media_resource, message: ["#{msg}. #{e}"]})
      end

      #
      # @param bulk_upload [BulkUpload] - instance of BulkUpload that is still marked as :in_progress in our local database
      def unsuppress_publish(bulk_upload)
        span = OpenTelemetry::Trace.current_span
        span.add_event("Begin unsuppress_publish step", attributes: {"bulk_upload" => bulk_upload.inspect})
        Logger.logger.info("Begin change suppress status step")
        media_resource = bulk_upload.media_resource
        suppress_obj = SuppressPublish.new(media_record: media_resource)
        suppress_obj.unsuppress
        if suppress_obj.errors.flatten.size > 0
          msg = "Failed to unsuppress publishing #{media_resource} with errors #{suppress_obj.errors.flatten}"
          Logger.logger.error(msg)
          Failure({media_resource: media_resource, message: suppress_obj.errors.flatten})
        else
          Logger.logger.info("Finish unsuppress publishing step")
          Success(bulk_upload)
        end
      rescue => e
        msg = "Failed to unsuppress publishing"
        e.backtrace
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        Logger.logger.error(msg)
        Failure({media_resource: media_resource, message: ["#{msg}. #{e}"]})
      end

      #
      # @param bulk_upload [BulkUpload] - instance of BulkUpload that is still marked as :in_progress in our local database
      def move(bulk_upload)
        span = OpenTelemetry::Trace.current_span
        span.add_event("Begin move transaction step", attributes: {"bulk_upload" => bulk_upload.inspect})
        media_resource = bulk_upload.media_resource
        Extract.new.move_processed_file(key: media_resource.original_filename, new_prefix: ENV.fetch("MEDIA_COMPLETED_KEY_PREFIX"))
        Success(media_resource)
      rescue => e
        msg = "Failed to update link Kaltura MediaResource to Alma/Primo"
        Logger.logger.info(msg)
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        Logger.logger.error(msg)
        Failure({media_resource: media_resource, message: ["#{msg}. #{e}"]})
      end
    end
  end
end
