require "dry-transaction"
require "opentelemetry/sdk"

module App
  module Transactions
    class Expire
      include Dry::Transaction
      def self.call(...)
        new.call(...)
      end

      step :delete_from_kaltura
      step :delete_from_filesystem
      step :expire # from the database
      step :suppress_in_alma # hide record in alma
      step :delete_link # from alma

      private

      def delete_from_kaltura(media_record)
        span = OpenTelemetry::Trace.current_span
        span.add_event("Begin delete_from_kaltura transaction step", attributes: {"media_resource" => media_record.inspect})
        KalturaUtils::Delete.for.delete(entry_id: media_record.entry_id)
        Success(media_record)
      rescue => e
        msg = "Failed to delete from kaltura #{media_record.inspect}"
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        Logger.logger.error(msg)
        Failure(["#{msg}\n\n#{e}"])
      end

      def delete_from_filesystem(media_record)
        span = OpenTelemetry::Trace.current_span
        span.add_event("Begin delete_from_filesystem transaction step", attributes: {"media_resource" => media_record.inspect})
        FileDelete.call(file_name: media_record.original_filename)
        Success(media_record)
      rescue => e
        msg = "Failed to delete file #{media_record.original_filename}"
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        Logger.logger.warn(msg)

        # we don't consider the inability to delete a local file a Failure. It could have already been removed for a
        # variety of reasons
        Success(media_record)
      end

      def expire(media_record)
        span = OpenTelemetry::Trace.current_span
        span.add_event("Begin expire transaction step", attributes: {"media_resource" => media_record.inspect})
        Logger.logger.info("Begin expire transaction step")

        media_record.expired = true
        media_record = media_record.save

        Success(media_record)
      rescue => e
        msg = "Failed to expire #{media_record.inspect} in local database"
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        Logger.logger.error(msg)
        Failure(["#{msg}\n\n#{e}"])
      end

      def suppress_in_alma(media_record)
        span = OpenTelemetry::Trace.current_span
        span.add_event("Begin suppress_in_alma transaction step", attributes: {"media_resource" => media_record.inspect})
        Logger.logger.info("Begin suppress_in_alma transaction step")
        suppress_obj = SuppressPublish.new(media_record: media_record)
        suppress_obj.suppress
        if suppress_obj.errors.flatten.size > 0
          msg = "#{media_record.inspect} has suppress_in_alma errors #{suppress_obj.errors.flatten}"
          Logger.logger.error(msg)
          Failure(suppress_obj.errors.flatten)
        else
          Success(media_record)
        end
      rescue => e
        msg = "Failed to suppress #{media_record.inspect} in Alma"
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        Logger.logger.error(msg)
        Failure(["#{msg}\n\n#{e}"])
      end

      def delete_link(media_record)
        span = OpenTelemetry::Trace.current_span
        span.add_event("Begin delete static url transaction step", attributes: {"media_resource" => media_record.inspect})
        Logger.logger.info("Begin delete static url transaction step")
        @transformer = Transform.new(file_name: media_record.original_filename)
        @transformer.extract_bib_record
        @transformer.extract_portfolio
        linker = App::Link.new(media_record: media_record, portfolios: @transformer.portfolios)
        linker.unlink
        if linker.errors.flatten.size > 0
          msg = "Failed to remove URL for #{media_record.inspect}"
          Logger.logger.error(msg)
          Failure({media_resource: media_record, message: linker.errors.flatten})
        else
          Success(media_record)
        end
      rescue => e
        msg = "Failed to delete static url #{media_record.inspect}"
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        Logger.logger.error(msg)
        Failure(["#{msg}\n\n#{e}"])
      end
    end
  end
end
