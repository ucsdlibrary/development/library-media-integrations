require "json"
require "net/http"
require "singleton"
require "uri"
require "kaltura"

module App
  module KalturaUtils
    class Delete
      include Singleton
      include Kaltura

      def initialize
        @client ||= ClientWrapper.client
      end

      ##
      # Return the Delete class to use for `delete_from_kaltura` step in App::Transactions::Expire
      def self.for
        if ENV["NULL_DELETE"]
          NullDelete.new
        else
          instance
        end
      end

      def delete(entry_id:)
        delete_media(entry_id: entry_id)
        Logger.logger.info("Successfully deleted media with Entry ID: #{entry_id}")
      rescue => e
        Logger.logger.error("Failed to delete from Kaltura")
        Logger.logger.error(e)
        raise
      end

      def delete_media(entry_id:)
        @client.media_service.delete(entry_id)
      end

      ## For use in local development to not send data to Kaltura API
      # Targets `NULL_DELETE` environment variable
      class NullDelete
        def delete(*args)
          Logger.logger.info("NullDelete: NOT deleting Payload from Kaltura")
        end
      end
    end
  end
end
