require "kaltura"

module App
  module KalturaUtils
    class ClientWrapper
      include Kaltura
      KALTURA_PARTNER_ID = "2323111".freeze
      KALTURA_SESSION_EXPIRATION_IN_SECONDS = 86400

      def self.client
        config = KalturaConfiguration.new
        config.service_url = ENV.fetch("KALTURA_SERVICE_URL")
        config.logger = Logger.logger
        @client ||= KalturaClient.new(config)
        @client.ks = @client.generate_session_v2(
          ENV.fetch("KALTURA_PW"),
          ENV.fetch("KALTURA_USER"),
          Kaltura::KalturaSessionType::ADMIN,
          KALTURA_PARTNER_ID,
          KALTURA_SESSION_EXPIRATION_IN_SECONDS,
          "*,disableentitlement"
        )
        @client
      rescue => e
        Logger.logger.error("Failed to create Kaltura client")
        Logger.logger.error(e)
      end
    end
  end
end
