require "kaltura"

module App
  module KalturaUtils
    class BulkUploadJob
      include Kaltura
      #
      # @param bulk_upload [BulkUpload] - instance of BulkUpload that is still marked as :in_progress in our local database
      # @return [KalturaBulkUpload] see: https://www.rubydoc.info/gems/kaltura-client/16.10.0/Kaltura/KalturaBulkService#get-instance_method
      def self.current_status(bulk_upload:)
        Logger.logger.info("Checking status for KalturaBulkUpload Job with id: #{bulk_upload.bulk_job_id}")
        ClientWrapper.client.bulk_service.get(bulk_upload.bulk_job_id)
      end

      # Helper methods to determine whether Kaltura considers a job as "finished" or not. Regardless of whether it succeeded or not.
      # @see: https://www.rubydoc.info/gems/kaltura-client/16.10.0/Kaltura/KalturaBatchJobStatus
      def self.succeeded?(status:)
        [Kaltura::KalturaBatchJobStatus::FINISHED].include?(status)
      end

      def self.failed?(status:)
        [
          Kaltura::KalturaBatchJobStatus::FAILED,
          Kaltura::KalturaBatchJobStatus::FATAL,
          Kaltura::KalturaBatchJobStatus::ABORTED
        ].include?(status)
      end

      def self.bulk_upload_entry_id(bulk_upload:)
        span = OpenTelemetry::Trace.current_span
        Logger.logger.info("Checking for bulk upload entry_id")
        media_resource = bulk_upload.media_resource
        filter = KalturaMediaEntryFilter.new
        filter.creator_id_equal = "lib-kaltura@ucsd.edu"
        filter.reference_id_equal = media_resource.reference_id
        pager = KalturaFilterPager.new

        results = ClientWrapper.client.media_service.list(filter, pager)
        if results.objects&.any?
          results.objects.first.id
        else
          msg = "No media entry found"
          Logger.logger.error(msg)
          raise StandardError, msg
        end
      rescue => e
        msg = "Failed to find entry_id: #{e.message}"
        Logger.logger.error(msg)
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        Failure(msg)
      end
    end
  end
end
