module App
  module KalturaUtils
    class PersistEntryId
      def self.call(media_record:, entry_id:)
        media_record.entry_id = entry_id
        media_record.save
      end
    end
  end
end
