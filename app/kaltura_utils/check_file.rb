require "kaltura"

module App
  module KalturaUtils
    class CheckFile
      include Kaltura
      KALTURA_CATEGORYID_UCSD_LIBRARY_AFFILIATE_ACCESS = "329962122".freeze
      PAGE_SIZE = 500

      def initialize
        @client ||= ClientWrapper.client
      end

      def self.check_kaltura_file(entry_id)
        new.check_kaltura_file_instance(entry_id)
      end

      def self.check_all_kaltura_files
        new.check_all_kaltura_files_instance
      end

      def check_kaltura_file_instance(entry_id)
        span = OpenTelemetry::Trace.current_span
        Logger.logger.info("Checking for kaltura media file with entry id: #{entry_id}")
        filter = KalturaMediaEntryFilter.new
        filter.id_equal = entry_id
        pager = KalturaFilterPager.new
        results = @client.media_service.list(filter, pager)
        if results.objects&.any?
          media_entry = results.objects.first
          Logger.logger.info("Found entry: Name: #{media_entry.name} ID: #{media_entry.id}")
          media_entry
        else
          msg = "No media entry found with entry_id #{entry_id}"
          Logger.logger.error(msg)
          raise StandardError, msg
        end
      rescue => e
        msg = "Failed to find media file entry_id #{entry_id}: #{e.message}"
        Logger.logger.error(msg)
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        Failure(msg)
      end

      def check_all_kaltura_files_instance
        span = OpenTelemetry::Trace.current_span
        Logger.logger.info("Checking for all kaltura media files in our category")
        filter = KalturaMediaEntryFilter.new
        filter.categories_ids_match_and = KALTURA_CATEGORYID_UCSD_LIBRARY_AFFILIATE_ACCESS
        pager = KalturaFilterPager.new
        pager.page_size = PAGE_SIZE
        results = @client.media_service.list(filter, pager)
        if results.objects&.any?
          results.objects
        else
          msg = "No media entry found"
          Logger.logger.error(msg)
          raise StandardError, msg
        end
      rescue => e
        msg = "Failed to find all media files: #{e.message}"
        Logger.logger.error(msg)
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        Failure(msg)
      end
    end
  end
end
