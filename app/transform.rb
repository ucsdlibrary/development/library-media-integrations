require "nokogiri"
require "opentelemetry/sdk"

module App
  class Transform
    attr_reader :file_name, :errors

    PORTFOLIOS_LIMIT = 100
    SERVICE_ID = "62249556810006535".freeze
    COLLECTION_ID = "61249556820006535".freeze

    # Filenames should take the form `human-readable-stuff-MMSID.ext`
    def self.file_name_to_mms_id(file_name:)
      file_name.split(".").first.split("-").last
    end

    ##
    # param file_name [String]
    def initialize(file_name:)
      @file_name = file_name
      @mms_id = self.class.file_name_to_mms_id(file_name: file_name)
      @errors = []
    end

    def flattened_errors
      errors.flatten
    end

    # Primary method to being Alma XML file extraction for mapping to bib record
    def extract_bib_record
      span = OpenTelemetry::Trace.current_span
      alma_url = "#{ENV.fetch("ALMA_API_URL")}/almaws/v1/bibs/#{@mms_id}?apikey=#{ENV.fetch("ALMA_API_KEY")}&format=xml"
      uri = URI(alma_url)
      res = Net::HTTP.get_response(uri)
      errors << extract_error(data: res.body) unless res.code == "200"
      transform_bib_record(data: res.body)
    rescue => e
      errors << e
      msg = "Failed to extract bib record for #{@mms_id}"
      span.status = OpenTelemetry::Trace::Status.error(msg)
      span.record_exception(e)
    end

    # Primary method to being Alma XML file extraction for mapping to portfolio record
    def extract_portfolio
      span = OpenTelemetry::Trace.current_span
      portfolio_id = lookup_portfolio_id
      return nil unless !portfolio_id.empty?
      alma_url = "#{ENV.fetch("ALMA_API_URL")}/almaws/v1/electronic/e-collections/#{COLLECTION_ID}/e-services/#{SERVICE_ID}/portfolios/#{portfolio_id}?apikey=#{ENV.fetch("ALMA_API_KEY")}&format=xml"
      uri = URI(alma_url)
      res = Net::HTTP.get_response(uri)
      errors << extract_error(data: res.body) unless res.code == "200"
      transform_portfolio(data: res.body)
    rescue => e
      errors << e
      msg = "Failed to extract portfolio for #{@mms_id}"
      span.status = OpenTelemetry::Trace::Status.error(msg)
      span.record_exception(e)
    end

    def lookup_portfolio_id
      span = OpenTelemetry::Trace.current_span
      items_count = PORTFOLIOS_LIMIT
      offset = 0
      portfolio_id = ""
      loop do
        alma_url = "#{ENV.fetch("ALMA_API_URL")}/almaws/v1/electronic/e-collections/#{COLLECTION_ID}/e-services/#{SERVICE_ID}/portfolios?apikey=#{ENV.fetch("ALMA_API_KEY")}&limit=#{PORTFOLIOS_LIMIT}&offset=#{offset}&format=xml"
        uri = URI(alma_url)
        res = Net::HTTP.get_response(uri)
        errors << res.body unless res.code == "200"
        alma_doc = Nokogiri::XML(res.body)
        alma_doc.css("portfolio").each do |i|
          if @mms_id == i.at_css("resource_metadata mms_id").text
            return i.at_css("id").text
          end
        end
        items_count = alma_doc.xpath("//portfolio").count
        offset += PORTFOLIOS_LIMIT
        break if items_count < PORTFOLIOS_LIMIT
      end
      portfolio_id
    rescue => e
      errors << e
      msg = "Failed to retrieve portfolio id for #{@mms_id}"
      span.status = OpenTelemetry::Trace::Status.error(msg)
      span.record_exception(e)
    end

    # Array that holds the set of Bib records
    def records
      @records ||= []
    end

    # Array that holds the set of Portfolios
    def portfolios
      @portfolios ||= []
    end

    private

    def transform_bib_record(data:)
      alma_doc = Nokogiri::XML(data)

      alma_doc.css("bib").each do |i|
        record = BibRecord.new
        errors << Rules.apply(type: BibRecordRule, record: record, xml_doc: i)
        records << record
      end
    end

    def transform_portfolio(data:)
      alma_doc = Nokogiri::XML(data)

      alma_doc.css("portfolio").each do |i|
        portfolio = Portfolio.new
        errors << Rules.apply(type: PortfolioRule, record: portfolio, xml_doc: i)
        portfolios << portfolio
      end
    end

    def extract_error(data:)
      error_info = []
      alma_doc = Nokogiri::XML(data)
      error_info << alma_doc.css("errorCode").text
      error_info << alma_doc.css("errorMessage").text
      error_info.flatten.join(" - ")
    end
  end
end
