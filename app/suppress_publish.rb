require "opentelemetry/sdk"
require "net/http"
require "uri"

module App
  class SuppressPublish
    attr_reader :media_record, :errors
    SUPPRESS_PUPLISHING_TRUE = "true".freeze
    SUPPRESS_PUPLISHING_FALSE = "false".freeze

    ##
    # param media_record [MediaResource]
    def initialize(media_record:)
      @media_record = media_record
      @errors = []
    end

    def suppress
      span = OpenTelemetry::Trace.current_span
      alma_url = "#{ENV.fetch("ALMA_API_URL")}/almaws/v1/bibs/#{@media_record[:reference_id]}?apikey=#{ENV.fetch("ALMA_API_KEY")}&format=xml"
      uri = URI(alma_url)
      res = Net::HTTP.get_response(uri)
      errors << res.body unless res.code == "200"
      change_suppress_status(data: res.body, mmsid: @media_record[:reference_id], status: SUPPRESS_PUPLISHING_TRUE)
    rescue => e
      errors << e
      msg = "Failed to suppress publishing for record #{@media_record[:name]} with mmsid: #{@media_record[:reference_id]}"
      span.status = OpenTelemetry::Trace::Status.error(msg)
      span.record_exception(e)
    end

    def unsuppress
      span = OpenTelemetry::Trace.current_span
      alma_url = "#{ENV.fetch("ALMA_API_URL")}/almaws/v1/bibs/#{@media_record[:reference_id]}?apikey=#{ENV.fetch("ALMA_API_KEY")}&format=xml"
      uri = URI(alma_url)
      res = Net::HTTP.get_response(uri)
      errors << res.body unless res.code == "200"
      change_suppress_status(data: res.body, mmsid: @media_record[:reference_id], status: SUPPRESS_PUPLISHING_FALSE)
    rescue => e
      errors << e
      msg = "Failed to unsuppress publishing for record #{@media_record[:name]} with mmsid: #{@media_record[:reference_id]}"
      span.status = OpenTelemetry::Trace::Status.error(msg)
      span.record_exception(e)
    end

    def change_suppress_status(data:, mmsid:, status:)
      tmp_status = (status == SUPPRESS_PUPLISHING_FALSE) ? SUPPRESS_PUPLISHING_TRUE : SUPPRESS_PUPLISHING_FALSE
      original_suppress_field = "<suppress_from_publishing>#{tmp_status}</suppress_from_publishing>"
      update_suppress_field = "<suppress_from_publishing>#{status}</suppress_from_publishing>"
      Logger.logger.info("Start to change suppress status from #{original_suppress_field} to #{update_suppress_field}")
      return data unless data.include?(original_suppress_field)
      Logger.logger.info("Original data:#{data}")
      data = data.gsub!(original_suppress_field, update_suppress_field)
      Logger.logger.info("Updated data:#{data}")
      uri = URI("#{ENV.fetch("ALMA_API_URL")}/almaws/v1/bibs/#{mmsid}?apikey=#{ENV.fetch("ALMA_API_KEY")}")
      req = Net::HTTP::Put.new(uri)
      req["Content-Type"] = "application/xml"
      req.body = data
      response = Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == "https") { |http|
        http.request(req)
      }
      Logger.logger.info("Successfuly updated status from #{original_suppress_field} to #{update_suppress_field}")
      errors << "Failed to post Data. Response Code: #{response.code}" if response.code != "200"
    rescue => e
      errors << e
    end
  end
end
