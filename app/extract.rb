require "opentelemetry/sdk"
require "fileutils"
require "dry/monads"
require_relative "logger"

##
# Connect to a Minio/S3 bucket which holds the Alma media files for Kaltura processing
module App
  class Extract
    include Dry::Monads[:result]
    SUPPORTED_EXTENSIONS = %w[.mp4 .mov .vtt .srt].freeze
    def self.call(input)
      new.call(input)
    end

    def call(input)
      span = OpenTelemetry::Trace.current_span
      Logger.logger.info("Initiating extraction of Alma media files")

      begin
        Logger.logger.info("Checking for valid file name")
        extensions_regex = SUPPORTED_EXTENSIONS.join("|")
        match_data = input.match(/^(.*?)-([0-9]+)(?:#{extensions_regex})$/)

        unless match_data
          Logger.logger.info("File name is invalid")
          msg = "Invalid file naming convention or unsupported file type for #{input}"
          raise StandardError, msg
        end
        Success(input)
      rescue => e
        msg = "Failed to extract #{input}: #{e.message}"
        Logger.logger.error(msg)
        errors_prefix = ENV.fetch("MEDIA_ERRORS_KEY_PREFIX")
        Logger.logger.info("Moving failed file #{input} to #{errors_prefix}")
        move_processed_file(key: input, new_prefix: errors_prefix)
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(e)
        Failure(msg)
      end
    end

    # Move a file once it is processed
    def move_processed_file(key:, new_prefix:)
      span = OpenTelemetry::Trace.current_span
      Logger.logger.info("Moving file #{key} to #{new_prefix}")
      span.add_event("Moving file #{key} to #{new_prefix}")
      FileUtils.move("#{ENV.fetch("MEDIA_FILES_PATH")}/#{key}", "#{ENV.fetch("MEDIA_FILES_PATH")}/#{new_prefix}")
    end
  end
end
