require "sequel"

module App
  class DbConfig
    DATABASE = {
      user: ENV.fetch("POSTGRESQL_USERNAME"),
      password: ENV.fetch("POSTGRESQL_PASSWORD"),
      host: ENV.fetch("POSTGRESQL_HOST"),
      port: ENV.fetch("POSTGRESQL_PORT"),
      database: ENV.fetch("POSTGRESQL_DATABASE"),
      adapter: "postgres"
    }.freeze
    # establish a database connection for the lifecycle of the application
    def self.connection
      @db ||= Sequel.connect(DATABASE)
    end
  end
end
