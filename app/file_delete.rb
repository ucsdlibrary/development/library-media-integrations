module App
  class FileDelete
    def self.call(file_name:)
      span = OpenTelemetry::Trace.current_span
      absolute_filepath = "#{ENV.fetch("MEDIA_FILES_PATH")}/#{ENV.fetch("MEDIA_COMPLETED_KEY_PREFIX")}#{file_name}"
      span.add_event("Deleting #{absolute_filepath} from filesystem")
      File.delete(absolute_filepath)
    end
  end
end
