##
# Responsible for persisting a MediaResource to the local database
module App
  class MediaPersister
    attr_reader :records, :portfolios, :file_name

    def initialize(records:, portfolios:, file_name:)
      @records = records
      @portfolios = portfolios
      @file_name = file_name
    end

    def persist
      return nil unless !records.empty? && !portfolios.empty?

      Logger.logger.info("Persisting MediaResource with MMS ID: #{records.first.mms_id}")
      Logger.logger.info("Data being persisted: #{records.first.title}, #{file_name}, #{records.first.author}, #{records.first.mms_id}, #{portfolios.first.available_from_date}, #{portfolios.first.available_until_date}, #{portfolios.first.static_url}, #{portfolios.first.access_type}")

      MediaResource.find_or_create(reference_id: records.first.mms_id) do |m|
        m.name = records.first.title
        m.original_filename = file_name
        m.creator_id = records.first.author
        m.reference_id = records.first.mms_id
        m.start_date = portfolios.first.available_from_date
        m.end_date = portfolios.first.available_until_date
        m.data_url = portfolios.first.static_url
        m.license_type = portfolios.first.access_type
      end.save(raise_on_failure: true)
    end
  end
end
