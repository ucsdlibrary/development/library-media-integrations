require "mail"

module App
  class EmailConfig
    App::Logger.logger.debug("Loading email configuration")
    case ENV.fetch("APP_ENV")
    when "development"
      require "letter_opener"
      # allow letter_opener to work in container
      # see: https://github.com/ryanb/letter_opener#remote-alternatives
      ENV["LAUNCHY_DRY_RUN"] = "true"
      ENV["BROWSER"] = File::NULL
      Mail.defaults do
        delivery_method LetterOpener::DeliveryMethod, location: File.expand_path("../../tmp/letter_opener", __FILE__)
      end
      App::Logger.logger.debug("Loaded email configuration for development - emails are written to tmp/letter_opener")
    when "production"
      Mail.defaults do
        delivery_method :smtp,
          address: ENV.fetch("EMAIL_SMTP_ADDRESS"),
          port: ENV.fetch("EMAIL_SMTP_PORT"),
          user_name: ENV.fetch("EMAIL_SMTP_USER_NAME"),
          password: ENV.fetch("EMAIL_SMTP_PASSWORD"),
          authentication: "login",
          enable_starttls_auto: true
      end
      App::Logger.logger.debug("Loaded email configuration for production")
    end
  end
end
