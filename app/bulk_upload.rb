require "kaltura"

module App
  class BulkUpload
    include Kaltura
    def initialize
      @client ||= KalturaUtils::ClientWrapper.client
    end

    def self.call(mrss_file:)
      new.call(mrss_file: mrss_file)
    end

    def call(mrss_file:)
      span = OpenTelemetry::Trace.current_span
      file_name = "bulkUpload.xml"
      Tempfile.create(file_name) do |file|
        file.write(mrss_file.to_s)
        file.rewind
        bulk_upload_data = KalturaBulkUploadXmlJobData.new
        bulk_upload_entry_data = KalturaBulkUploadEntryData.new
        @client.media_service.bulk_upload_add(file, bulk_upload_data, bulk_upload_entry_data)
      end
    rescue => e
      msg = "Failed to run bulk upload process"
      Logger.logger.error(msg)
      span.status = OpenTelemetry::Trace::Status.error(msg)
      span.record_exception(e)
      Failure(msg)
    end
  end
end
