require "csv"
require "erb"
require "mail"
require "opentelemetry/sdk"

module App
  class Email
    BODY_TEMPLATE = ERB.new <<-EOF
      Description: <%= description %>
      <% if additional_fields %>
      <% additional_fields.each do |k,v| %>
      <%= k.to_s.split('_').map(&:capitalize).join(' ') %>: <%= v %>
      <% end %>

      <% end %>

      MMS ID: <%= mms_id %>
      Filename: <%= file_name %>
      <% if errors %>
      Errors: <%= errors %>
      <% end %>
    EOF

    ##
    # @param subject [String]
    # @param description [String]
    # @param mms_id [String]
    # @param file_name [String]
    def self.send(subject:, description:, mms_id:, file_name:, errors: nil, to: ENV.fetch("EMAIL_TO"), **additional_fields)
      span = OpenTelemetry::Trace.current_span
      sending_msg = "Sending email to #{to} for MMSID: #{mms_id} with description: #{description}"
      Logger.logger.info(sending_msg)
      span.add_event(sending_msg)

      Mail.deliver do
        from ENV.fetch("EMAIL_FROM")
        to Array(to).join(",")
        subject subject
        body BODY_TEMPLATE.result(binding)
      end
      sent_msg = "Email delivered to #{to} for MMSID: #{mms_id} with description: #{description}"
      Logger.logger.info(sent_msg)
      span.add_event(sent_msg)
    rescue => e
      msg = "Failed to send email to #{to} for MMSID: #{mms_id} with description: #{description}"
      Logger.logger.error(msg)
      span.status = OpenTelemetry::Trace::Status.error(msg)
      span.record_exception(e)
    end
  end
end
