require "opentelemetry/sdk"
require "net/http"
require "uri"
require "kaltura"

module App
  class Link
    attr_reader :media_record, :portfolios, :errors
    include Kaltura

    ##
    # param media_record [MediaResource]
    def initialize(media_record:, portfolios:)
      @media_record = media_record
      @portfolios = portfolios
      @errors = []
    end

    def link
      span = OpenTelemetry::Trace.current_span
      Logger.logger.info("Checking media record")
      if @media_record[:data_url] != ""
        Logger.logger.info("Static URL is already set: #{@media_record[:data_url]}")
        Logger.logger.info("Overwriting static URL now")
      else
        Logger.logger.info("Data url is empty, updating")
      end
      name = @media_record.name.include?("/") ? @media_record.name.tr!("/", "_") : @media_record.name
      name = URI.encode_www_form_component(name)
      static_url = "jkey=https://mediaspace.ucsd.edu/media/#{name}/#{@media_record.entry_id}/329962122"
      Logger.logger.info("Updating static URL to: #{static_url}")
      portfolio = @portfolios.first
      portfolio_id = portfolio.id
      alma_url = "#{ENV.fetch("ALMA_API_URL")}/almaws/v1/electronic/e-collections/#{App::Transform::COLLECTION_ID}/e-services/#{App::Transform::SERVICE_ID}/portfolios/#{portfolio_id}?apikey=#{ENV.fetch("ALMA_API_KEY")}&format=xml"
      Logger.logger.info("Alma url set to update: #{alma_url}")
      uri = URI(alma_url)
      res = Net::HTTP.get_response(uri)
      if res.code != "200"
        errors << res.body
        Logger.logger.error("Failed to fetch portfolio data from Alma: #{res.body}")
        return nil
      end
      Logger.logger.info("Urls set, updating...")
      @media_record.data_url = static_url
      @media_record = @media_record.save
      update_static_url(data: res.body, mmsid: @media_record[:reference_id], static_url: static_url)
    rescue => e
      errors << e
      msg = "Failed to link Kaltura URL for record #{@media_record[:name]} with mmsid: #{@media_record[:reference_id]}
      and alma url #{alma_url}"
      Logger.logger.info(msg)
      span.status = OpenTelemetry::Trace::Status.error(msg)
      span.record_exception(e)
    end

    def unlink
      span = OpenTelemetry::Trace.current_span
      Logger.logger.info("Checking media record")
      if @media_record[:data_url] != ""
        Logger.logger.info("Static URL is already set: #{@media_record[:data_url]}")
        Logger.logger.info("Removing static URL now")
      else
        Logger.logger.info("Data url is empty, no need to update")
      end
      name = @media_record.name.include?("/") ? @media_record.name.tr!("/", "_") : @media_record.name
      name = URI.encode_www_form_component(name)
      static_url = "jkey=https://mediaspace.ucsd.edu/media/#{name}/#{@media_record.entry_id}/329962122"
      Logger.logger.info("Removing current static URL - #{static_url}")
      portfolio = @portfolios.first
      portfolio_id = portfolio.id
      alma_url = "#{ENV.fetch("ALMA_API_URL")}/almaws/v1/electronic/e-collections/#{App::Transform::COLLECTION_ID}/e-services/#{App::Transform::SERVICE_ID}/portfolios/#{portfolio_id}?apikey=#{ENV.fetch("ALMA_API_KEY")}&format=xml"
      Logger.logger.info("Alma url set to remove: #{alma_url}")
      uri = URI(alma_url)
      res = Net::HTTP.get_response(uri)
      if res.code != "200"
        errors << res.body
        Logger.logger.error("Failed to fetch portfolio data from Alma: #{res.body}")
        return nil
      end
      Logger.logger.info("Urls set, removing...")
      @media_record.data_url = ""
      @media_record = @media_record.save
      remove_static_url(data: res.body, mmsid: @media_record[:reference_id], static_url: static_url)
    rescue => e
      errors << e
      msg = "Failed to link Kaltura URL for record #{@media_record[:name]} with mmsid: #{@media_record[:reference_id]}
      and alma url #{alma_url}"
      Logger.logger.info(msg)
      span.status = OpenTelemetry::Trace::Status.error(msg)
      span.record_exception(e)
    end

    def update_static_url(data:, mmsid:, static_url:)
      original_static_url_field = /<static_url>.*?<\/static_url>/
      update_static_url_field = "<static_url>#{static_url}</static_url>"
      original_url_type_field = /<url_type>.*?<\/url_type>/
      update_url_type_field = "<url_type>static</url_type>"
      portfolio = @portfolios.first
      portfolio_id = portfolio.id
      data = data.gsub(original_static_url_field, update_static_url_field)
      data = data.gsub(original_url_type_field, update_url_type_field)
      uri = URI("#{ENV.fetch("ALMA_API_URL")}/almaws/v1/bibs/#{mmsid}/portfolios/#{portfolio_id}?apikey=#{ENV.fetch("ALMA_API_KEY")}")
      req = Net::HTTP::Put.new(uri)
      req["Content-Type"] = "application/xml"
      req.body = data
      response = Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == "https") { |http|
        http.request(req)
      }
      errors << "Failed to post Data. Response Code: #{response.code} #{response.message} - #{response.body}" if response.code != "200"
      data
    rescue => e
      errors << e
      Logger.logger.info(errors)
    end

    def remove_static_url(data:, mmsid:, static_url:)
      update_static_url_field = "<static_url></static_url>"
      original_static_url_field = "<static_url>#{static_url}</static_url>"
      portfolio = @portfolios.first
      portfolio_id = portfolio.id
      data = data.gsub(original_static_url_field, update_static_url_field) if data.include?(original_static_url_field)
      uri = URI("#{ENV.fetch("ALMA_API_URL")}/almaws/v1/bibs/#{mmsid}/portfolios/#{portfolio_id}?apikey=#{ENV.fetch("ALMA_API_KEY")}")
      req = Net::HTTP::Put.new(uri)
      req["Content-Type"] = "application/xml"
      req.body = data
      response = Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == "https") { |http|
        http.request(req)
      }
      errors << "Failed to post Data. Response Code: #{response.code} #{response.message} - #{response.body}" if response.code != "200"
      data
    rescue => e
      errors << e
      Logger.logger.info(errors)
    end
  end
end
