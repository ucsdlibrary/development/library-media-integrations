module App
  class CsvParser
    # Expected format of CSV is a list of mmsid's
    # @param file_name String
    def self.call(file_name:)
      span = OpenTelemetry::Trace.current_span
      span.add_event("Parsing CSV file: #{file_name} from filesystem")
      CSV.read(file_name, headers: false).flatten
    end
  end
end
