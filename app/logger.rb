require "ecs_logging/logger"

module App
  class Logger
    def self.logger
      @logger = EcsLogging::Logger.new(
        $stdout,
        level: ENV.fetch("LOGGER_LEVEL", "info").to_sym
      )
    end
  end
end
