require "opentelemetry/sdk"
require "opentelemetry/instrumentation/all"
require "opentelemetry-exporter-otlp"
require_relative "app"

# don't buffer log output
$stdout.sync = true

# OpenTelemetry configuration
# see: https://opentelemetry.io/docs/languages/ruby/getting-started/#instrumentation
OpenTelemetry::SDK.configure do |c|
  c.logger = App::Logger.logger
  c.service_name = "library-media-integrations"
  c.use_all # enables all instrumentation!
end

# setup tracer for the application
# see: https://opentelemetry.io/docs/languages/ruby/instrumentation/#traces
tracer_provider = OpenTelemetry.tracer_provider
LibraryMediaTracer = tracer_provider.tracer("LibraryMediaIntegrationsTracer")

# Run the scrip
LibraryMediaTracer.in_span("Metadata Update Check Transaction") do |span|
  App::Seed.new.seed_csv if ENV.fetch("APP_ENV") == "development"
  Dir["#{ENV.fetch("MEDIA_FILES_PATH")}/#{ENV.fetch("MEDIA_UPDATES_KEY_PREFIX")}*.csv"].each do |csv_file|
    next if File.directory?(csv_file)
    mmsids = App::CsvParser.call(file_name: csv_file)
    error_mmsids = []
    mmsids.each do |mmsid|
      App::Transactions::MetadataUpdateCheck.call(mmsid) do |result|
        result.success do |media_resource|
          streaming_link = media_resource.data_url.include?("jkey=") ? media_resource.data_url.gsub!("jkey=", "") : ""
          msg = "Portfolio Metadata Update successfully completed for #{media_resource.reference_id}"
          App::Logger.logger.info(msg)
          App::Email.send(subject: msg.gsub!("successfully ", ""),
            description: msg,
            mms_id: media_resource.reference_id,
            kaltura_entry_id: media_resource.entry_id,
            streaming_link: streaming_link,
            file_name: media_resource.original_filename)
        end
        result.failure do |failure|
          error_mmsids << mmsid
          msg = "Portfolio Metadata Update failed to complete for #{mmsid}"
          description = "Portfolio Metadata Update failed. #{failure[:message].flatten.join(", ")}"
          App::Logger.logger.error(msg)
          App::Logger.logger.error(failure)
          span.status = OpenTelemetry::Trace::Status.error(msg)
          span.record_exception(failure) if failure.is_a? Exception
          media_resource = failure[:media_resource]
          entry_id = media_resource ? media_resource.entry_id : ""
          data_url = media_resource ? media_resource.data_url : ""
          file_name = media_resource ? media_resource.original_filename : ""
          streaming_link = data_url.include?("jkey=") ? data_url.gsub!("jkey=", "") : ""
          App::Email.send(subject: msg,
            description: description,
            mms_id: mmsid,
            kaltura_entry_id: entry_id,
            streaming_link: streaming_link,
            file_name: file_name)
        end
      end
    end
    if error_mmsids.empty?
      App::Logger.logger.info("Moving CSV file #{csv_file}")
      span.add_event("Moving CSV file to completed/updates subdirectory", attributes: {"filename" => csv_file})
      FileUtils.move(csv_file, "#{ENV.fetch("MEDIA_FILES_PATH")}/#{ENV.fetch("MEDIA_UPDATES_COMPLETED_KEY_PREFIX")}")
    end
  end
end

# shutdown/close the tracer_provider
tracer_provider.shutdown
