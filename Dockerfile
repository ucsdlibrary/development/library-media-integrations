ARG IMAGE_REGISTRY='docker.io/'
FROM ${IMAGE_REGISTRY}ruby:3.3.5-alpine3.19 AS development

RUN apk --no-cache upgrade && \
  apk add --no-cache \
  build-base \
  postgresql-dev \
  less \
  gcompat \
  git

WORKDIR /library-media-integrations
ENV LANG=C.UTF-8
ENV APP_ENV=development
ENV LOGGER_LEVEL=debug

COPY Gemfile* ./
RUN bundle check || bundle install --jobs "$(nproc)"

COPY . ./

CMD ["ruby", "publish.rb"]
ENTRYPOINT ["/bin/sh", "bin/entrypoint.sh"]

FROM development AS production

ENV APP_ENV=production
ENV LOGGER_LEVEL=info

RUN bundle config set without 'development' \
  && bundle clean --force \
  # Remove unneeded files (cached *.gem, *.o, *.c)
  && find /usr/local/bundle/ -name "*.gem" -delete \
  && find /usr/local/bundle/ -name "*.c" -delete \
  && find /usr/local/bundle/ -name "*.o" -delete
