require "spec_helper"

RSpec.describe App::MediaFiles do
  describe ".validate" do
    it "returns an empty list of valid files with an invalid directory" do
      expect(described_class.validate(filepath: "/not/valid/filepath", errors_prefix: "")).to eq([])
    end

    context "with a directory of valid and invalid files" do
      let(:filepath) { Dir.tmpdir }
      let(:errors_prefix) { Dir.tmpdir + "/errors" }
      let(:valid_file_mp4) { "valid-file-123.mp4" }
      let(:valid_file_mov) { "valid-file-123.mov" }
      let(:invalid_file_mp3) { "invalid-file-123.mp3" }
      let(:invalid_file_missing_mmsid) { "invalid-file.mp4" }
      before do
        FileUtils.mkdir_p(errors_prefix)
        FileUtils.touch(File.join(filepath, valid_file_mp4))
        FileUtils.touch(File.join(filepath, valid_file_mov))
        FileUtils.touch(File.join(filepath, invalid_file_mp3))
        FileUtils.touch(File.join(filepath, invalid_file_missing_mmsid))
      end

      after do
        File.delete(File.join(filepath, valid_file_mp4))
        File.delete(File.join(filepath, valid_file_mov))
        FileUtils.rm_rf(errors_prefix)
      end

      it "only returns a list of valid filenames" do
        results = described_class.validate(filepath: filepath, errors_prefix: errors_prefix)
        expect(results.size).to eq(2)
        expect(results).to include(valid_file_mp4, valid_file_mov)
        expect(results).not_to include(invalid_file_mp3, invalid_file_missing_mmsid)
      end

      it "moves invalid filenames to errors subdirectory" do
        described_class.validate(filepath: filepath, errors_prefix: errors_prefix)
        expect(Dir.each_child(errors_prefix).count).to eq(2)
        expect(File.exist?(File.join(errors_prefix, invalid_file_mp3))).to be true
        expect(File.exist?(File.join(errors_prefix, invalid_file_missing_mmsid))).to be true
      end
    end
  end
end
