require "spec_helper"

RSpec.describe App::FileDelete do
  let(:media_files_path_completed) { ENV.fetch("MEDIA_FILES_PATH") + "/" + ENV.fetch("MEDIA_COMPLETED_KEY_PREFIX") }
  let(:original_filename) { "a-video-mms_id1234-1.mp4" }

  describe ".call" do
    context "when the file exists in MEDIA_COMPLETED_KEY_PREFIX" do
      let(:file) { Tempfile.create(original_filename, media_files_path_completed).tap { |f| f.write("something") } }
      let(:tmp_filename) { File.basename(file.path) }
      it "deletes the file from the filesystem" do
        expect { File.open(file.path) }.to_not raise_error
        described_class.call(file_name: tmp_filename)
        expect { File.open(file.path) }.to raise_error(Errno::ENOENT)
      end
    end

    context "when the file does not exist in MEDIA_COMPLETED_KEY_PREFIX" do
      it "throws a file not found error" do
        expect { described_class.call(file_name: original_filename) }.to raise_error(Errno::ENOENT)
      end
    end
  end
end
