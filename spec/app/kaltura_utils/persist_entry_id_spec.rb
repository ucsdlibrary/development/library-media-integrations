require "spec_helper"

RSpec.describe App::KalturaUtils::PersistEntryId do
  let(:entry_id) { "1_3bux4j0v" }
  let(:media_record) do
    MediaResource.new(
      name: "name",
      original_filename: "a-video-mms_id1234-1.mp4",
      creator_id: "creator_id-1",
      reference_id: "mms_id1234-1",
      data_url: "http://data-url.com",
      license_type: "perpetual",
      start_date: Date.new(2024, 2, 3)
    )
  end

  it "persists entry_id into MediaResource record" do
    described_class.call(media_record: media_record, entry_id: entry_id)
    expect(media_record.entry_id).to eq entry_id
  end
end
