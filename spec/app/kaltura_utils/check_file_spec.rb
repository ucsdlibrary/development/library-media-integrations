require "spec_helper"

RSpec.describe App::KalturaUtils::CheckFile do
  let(:entry_id) { "1_3bux4j0v" }
  let(:client) { instance_double("Kaltura::KalturaClient") }
  let(:media_service) { instance_double("Kaltura::KalturaMediaService") }
  let(:media_entry) { double("Kaltura::KalturaMediaEntry", id: entry_id, name: "name") }
  let(:list_response) do
    double("Kaltura::KalturaMediaListResponse", objects: [double("Kaltura::KalturaMediaEntry", id: entry_id, name: "name")])
  end

  before do
    allow(App::KalturaUtils::ClientWrapper).to receive(:client).and_return(client)
    allow(client).to receive(:media_service).and_return(media_service)
    allow(media_service).to receive(:list).and_return(list_response)
  end

  it "returns success when the entry is found" do
    result = described_class.check_kaltura_file(entry_id)
    expect(result.id).to eq(media_entry.id)
    expect(result.name).to eq(media_entry.name)
  end
end
