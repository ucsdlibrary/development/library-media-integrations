require "spec_helper"

RSpec.describe App::KalturaUtils::Delete do
  let(:entry_id) { "1_zquovb4w" }
  let(:client) { instance_double("Kaltura::KalturaClient") }
  let(:media_service) { instance_double("Kaltura::KalturaMediaService") }
  let(:null_delete) { App::KalturaUtils::Delete::NullDelete.new }

  before do
    allow(App::KalturaUtils::ClientWrapper).to receive(:client).and_return(client)
    allow(client).to receive(:media_service).and_return(media_service)
  end

  describe "NullDelete for local development" do
    before do
      stub_const("ENV", ENV.to_hash.merge("NULL_DELETE" => "yes"))
    end

    it "can use NullDelete when NULL_DELETE env var is present" do
      expect { described_class.for.delete(entry_id: entry_id) }.to output(/NullDelete/).to_stdout
    end
  end

  describe "interacting with Kaltura API endpoints" do
    before do
      allow(media_service).to receive(:delete).with(entry_id).and_return(true)
    end

    it "can delete media" do
      expect(described_class.instance.delete(entry_id: entry_id)).to be_truthy
    end
  end
end
