require "spec_helper"
require "dry/monads"

RSpec.describe App::Extract do
  subject { described_class.new }
  let(:media_files_path) { ENV.fetch("MEDIA_FILES_PATH") }
  let(:errors_prefix) { ENV.fetch("MEDIA_ERRORS_KEY_PREFIX") }
  let(:completed_prefix) { ENV.fetch("MEDIA_COMPLETED_KEY_PREFIX") }
  let(:pending_prefix) { ENV.fetch("MEDIA_PENDING_KEY_PREFIX") }
  let(:valid_media_type) { "valid-media-123.mp4" }
  let(:invalid_media_type) { "invalid.txt" }
  let(:completed_path) { "#{media_files_path}/#{completed_prefix}" }
  let(:errors_path) { "#{media_files_path}/#{errors_prefix}" }

  describe ".call" do
    context "when processing valid media files" do
      after do
        File.delete("#{media_files_path}/#{valid_media_type}") if File.exist?("#{media_files_path}/#{valid_media_type}")
      end
      it "processes and calls move_processed_file" do
        valid_file_path = File.join(media_files_path, valid_media_type)
        FileUtils.touch(valid_file_path)

        result = App::Extract.call(valid_media_type)

        expect(result).to be_a(Dry::Monads::Result::Success)
      end
    end

    context "when processing invalid media files" do
      after do
        File.delete("#{errors_path}/#{invalid_media_type}") if File.exist?("#{errors_path}/#{invalid_media_type}")
      end
      it "fails and calls move_processed_file with error" do
        # Create the file with FileUtils.touch
        invalid_file_path = File.join(media_files_path, invalid_media_type)
        FileUtils.touch(invalid_file_path)

        result = App::Extract.call(invalid_media_type)

        expect(result).to be_a(Dry::Monads::Result::Failure)
      end
    end

    describe "#move_processed_file" do
      let(:file_name) { "test-1234.mp4" }
      let(:file) { Tempfile.new(file_name, media_files_path).tap { |f| f.write("something") } }
      after do
        File.delete("#{media_files_path}/#{completed_prefix}#{file_name}") if File.exist?("#{media_files_path}/#{completed_prefix}#{file_name}")
      end
      it "moves the file to new_prefix location" do
        file_name = File.basename(file.path)
        new_file_path = "#{media_files_path}/#{completed_prefix}#{file_name}"

        expect { File.open(file.path) }.to_not raise_error
        expect(file.path).to eq("#{media_files_path}/#{file_name}")

        subject.move_processed_file(key: file_name, new_prefix: completed_prefix)
        expect { File.open(file.path) }.to raise_error(Errno::ENOENT)
        expect { File.open(new_file_path) }.to_not raise_error
        File.delete(new_file_path)
      end
    end
  end
end
