require "spec_helper"
RSpec.describe App::GenerateMrss do
  describe ".call" do
    let(:schema_file) { "spec/fixtures/bulkUploadXml.bulkUploadXML.xsd" }
    let(:schema) { Nokogiri::XML::Schema(File.read(schema_file)) }
    let(:media_record1) do
      MediaResource.new(
        name: "name",
        original_filename: "a-video-mms_id1234-1.mp4",
        creator_id: "creator_id-1",
        reference_id: "mms_id1234-1",
        data_url: "http://data-url.com",
        license_type: "perpetual",
        start_date: Date.new(2024, 2, 3),
        end_date: Date.new(2028, 2, 3)
      )
    end

    let(:media_record2) do
      MediaResource.new(
        name: "name2",
        original_filename: "a-video-mms_id1234-2.mp4",
        creator_id: "creator_id-2",
        reference_id: "mms_id1234-1",
        data_url: "http://data-url.com",
        license_type: "current",
        start_date: Date.new(2024, 2, 3),
        end_date: Date.new(2028, 2, 3)
      )
    end
    let(:media_record3) do
      MediaResource.new(
        name: "name3",
        original_filename: "a-video-mms_id1234-3.mp4",
        creator_id: "creator_id-1",
        reference_id: "mms_id1234-1",
        data_url: "http://data-url.com",
        license_type: "perpetual",
        start_date: Date.new(2024, 2, 3)
      )
    end

    it "generates valid xml output that conforms to the bulkUploadXML.xsd schema" do
      mrss_xml = described_class.call(media_resources: [media_record1, media_record2, media_record3])
      # uncomment to see what fails validation if changes are made
      # schema.validate(Nokogiri::XML(mrss_xml)).each {|e| puts e.message }
      expect(schema.valid?(Nokogiri::XML(mrss_xml))).to be true
    end
  end
end
