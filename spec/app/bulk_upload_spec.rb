require "spec_helper"
RSpec.describe App::BulkUpload do
  describe ".call" do
    let(:media_record) do
      MediaResource.create(
        name: "name",
        original_filename: "a-video-mms_id1234-1.mp4",
        creator_id: "creator_id-1",
        reference_id: "mms_id1234-1",
        data_url: "http://data-url.com",
        license_type: "perpetual",
        start_date: Date.new(2024, 2, 3)
      )
    end

    let(:bulk_upload) do
      BulkUpload.create(
        bulk_job_id: "29105704152",
        status: :in_progress
      )
    end

    let(:id) { "12345678910" }
    let(:client) { instance_double("Kaltura::KalturaClient") }
    let(:media_service) { instance_double("Kaltura::KalturaMediaService") }
    let(:media_entry) { double("Kaltura::KalturaMediaEntry", id: entry_id, name: "name") }
    let(:list_response) do
      double("Kaltura::KalturaBulkUpload", id: id)
    end

    before do
      allow(App::KalturaUtils::ClientWrapper).to receive(:client).and_return(client)
      allow(client).to receive(:media_service).and_return(media_service)
      allow(media_service).to receive(:bulk_upload_add).and_return(list_response)
    end

    before do
      bulk_upload.media_resource = media_record
    end

    it "generates valid KalturaBulkUpload xml response" do
      mrss_xml = App::GenerateMrss.call(media_resources: [media_record])
      expect(bulk_upload.media_resource.reference_id).to eq("mms_id1234-1")
      result = described_class.call(mrss_file: mrss_xml)
      expect(result).to eq(list_response)
      expect(result.id).to eq(id)
    end
  end
end
