require "spec_helper"

RSpec.describe App::CsvParser do
  let(:csv_fixture) { "#{ENV.fetch("MEDIA_FILES_PATH")}/#{ENV.fetch("MEDIA_UPDATES_KEY_PREFIX")}valid-updates.csv" }

  describe ".call" do
    context "when the file exists in MEDIA_UPDATES_KEY_PREFIX" do
      it "parses the CSV file from the filesystem" do
        expect(described_class.call(file_name: csv_fixture)).to contain_exactly("mmsid1", "mmsid2", "mmsid3")
      end
    end

    context "when the file does not exist in MEDIA_UPDATES_KEY_PREFIX" do
      it "throws a file not found error" do
        expect { described_class.call(file_name: "i-dont-exist.csv") }.to raise_error(Errno::ENOENT)
      end
    end
  end
end
