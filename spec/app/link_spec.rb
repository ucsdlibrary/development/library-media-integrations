require "spec_helper"

RSpec.describe App::Link do
  let(:api_key) { ENV.fetch("ALMA_API_KEY") }
  let(:api_uri) { ENV.fetch("ALMA_API_URL") }
  let(:collection_id) { App::Transform::COLLECTION_ID }
  let(:service_id) { App::Transform::SERVICE_ID }
  let(:response_body) { File.read("spec/fixtures/portfolioMissingStaticUrl.xml") }
  let(:response_body_updated) { File.read("spec/fixtures/updateAlmaStaticUrl.xml") }
  let(:response_body_with_static) { File.read("spec/fixtures/sampleAlmaSinglePortfolioXml.xml") }
  let(:headers) do
    {
      "Accept" => "*/*",
      "Accept-Encoding" => "gzip;q=1.0,deflate;q=0.6,identity;q=0.3",
      "Host" => "api-example.com",
      "User-Agent" => "Ruby"

    }
  end
  let(:api_headers) do
    {
      "Authorization" => "Bearer 1234",
      "Content-Type" => "application/xml"
    }
  end
  let!(:media_record) do
    MediaResource.create(
      name: "name",
      original_filename: "a-video-mms_id1234-1.mp4",
      creator_id: "creator_id-1",
      reference_id: "991020477189706535",
      data_url: "",
      license_type: "current",
      start_date: Date.new(2024, 2, 3),
      end_date: Date.new(2024, 3, 3),
      entry_id: "1_a2b3c4",
      expired: false
    ).save
  end
  let(:portfolios) do
    [
      Portfolio.new(
        "991020477189706535",
        Date.new(2024, 2, 3),
        Date.new(2024, 3, 3),
        "current",
        "",
        "53272118260006535"
      )
    ]
  end
  let(:request_url) { "#{api_uri}/almaws/v1/electronic/e-collections/#{collection_id}/e-services/#{service_id}/portfolios/#{portfolios.first.id}?apikey=#{api_key}&format=xml" }
  let(:name) { media_record.name.include?("/") ? media_record.name.tr!("/", "_") : media_record.name }
  let(:static_url) { "jkey=https://mediaspace.ucsd.edu/media/#{URI.encode_www_form_component(name)}/#{media_record.entry_id}/329962122" }
  let(:put_url) { "#{api_uri}/almaws/v1/bibs/#{media_record.reference_id}/portfolios/#{portfolios.first.id}?apikey=#{api_key}" }
  let(:subject) { described_class.new(media_record: media_record, portfolios: portfolios) }

  before do
    stub_const("ENV", ENV.to_hash.merge("ALMA_API_URL" => "http://api-example.com"))
    stub_const("ENV", ENV.to_hash.merge("ALMA_API_KEY" => "some-key"))
    stub_request(:get, request_url).with(headers: headers).to_return(status: 200, body: response_body, headers: {})
    stub_request(:put, put_url).to_return(status: 200, body: response_body_updated, headers: {})
  end

  context "when static_url is empty" do
    it "updates the static_url in Alma" do
      expect(subject.link).to include("<static_url>#{static_url}</static_url>")
    end
  end

  context "when static_url is not empty" do
    before do
      stub_request(:get, request_url).with(headers: headers).to_return(status: 200, body: response_body_with_static, headers: {})
      allow(subject).to receive(:update_static_url).and_call_original
    end

    it "overrides the existing static_url with a new one" do
      expect(subject.link).to include("<static_url>#{static_url}</static_url>")
    end
  end

  context "remove static_url when it is not empty " do
    before do
      stub_request(:get, request_url).with(headers: headers).to_return(status: 200, body: response_body, headers: {})
      allow(subject).to receive(:remove_static_url).and_call_original
    end

    it "overrides the existing static_url with a new one" do
      expect(subject.unlink).to include("<static_url></static_url>")
    end
  end
end
