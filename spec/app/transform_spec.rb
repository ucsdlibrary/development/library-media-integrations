require "spec_helper"

RSpec.describe App::Transform do
  let(:api_key) { ENV.fetch("ALMA_API_KEY") }
  let(:api_uri) { ENV.fetch("ALMA_API_URL") }
  let(:response_body) { File.open("spec/fixtures/sampleAlmaBibRecordXml.xml") }
  let(:headers) do
    {
      "Accept" => "*/*",
      "Accept-Encoding" => "gzip;q=1.0,deflate;q=0.6,identity;q=0.3",
      "Host" => "api-example.com",
      "User-Agent" => "Ruby"

    }
  end
  let(:api_headers) do
    {
      "Authorization" => "Bearer 1234",
      "Content-Type" => "application/xml"
    }
  end
  let(:file_name) { "sample-991020477189706535.mp4" }
  let(:portfolio_id) { "53272118260006535" }
  let(:request_url) { "#{api_uri}/almaws/v1/bibs/991020477189706535?apikey=#{api_key}&format=xml" }
  let(:subject) { described_class.new(file_name: file_name) }

  before do
    stub_const("ENV", ENV.to_hash.merge("ALMA_API_URL" => "http://api-example.com"))
    stub_const("ENV", ENV.to_hash.merge("ALMA_API_KEY" => "some-key"))
    allow(subject).to receive(:lookup_portfolio_id).and_return(portfolio_id)
  end

  context "with valid mms_id" do
    describe "#extract_bib_record" do
      before do
        stub_request(:get, request_url).with(headers: headers).to_return(status: 200, body: response_body, headers: {})
        stub_request(:get, api_uri).with(body: {"{}" => nil}, headers: api_headers)
        subject.extract_bib_record
      end

      it "assigns values to a Bib record" do
        expect(subject.records.first.mms_id).to eq("991020477189706535")
        expect(subject.records.first.title).to_not eq("Blueness / produced by Firelight Media")
        expect(subject.records.first.title).to eq("Blueness")
        expect(subject.records.first.author).to eq("Schroedinger, Kerstin")
        expect(subject.records.first.suppress_from_publishing).to eq("false")
      end

      it "tracks all Bib records" do
        expect(subject.records.count).to be(1)
      end
    end

    describe "#extract_portfolio" do
      let(:response_body) { File.open("spec/fixtures/sampleAlmaSinglePortfolioXml.xml") }
      let(:request_url) { "#{api_uri}/almaws/v1/electronic/e-collections/61249556820006535/e-services/62249556810006535/portfolios/#{portfolio_id}?apikey=#{api_key}&format=xml" }

      before do
        stub_request(:get, request_url).with(headers: headers).to_return(status: 200, body: response_body, headers: {})
        stub_request(:get, api_uri).with(body: {"{}" => nil}, headers: api_headers)
        subject.extract_portfolio
      end

      it "assigns values to a Portfolio record" do
        expect(subject.portfolios.first.id).to eq(portfolio_id)
        expect(subject.portfolios.first.mms_id).to eq("991020477189706535")
        expect(subject.portfolios.first.available_from_date).to eq("2024-03-01Z")
        expect(subject.portfolios.first.available_until_date).to eq("2024-05-31Z")
        expect(subject.portfolios.first.access_type).to eq("current")
        expect(subject.portfolios.first.static_url).to eq("jkey=https://mediaspace.ucsd.edu/media/Blueness_AD/1_pfdy9n9b")
      end

      it "tracks all Portfolio records" do
        expect(subject.portfolios.count).to be(1)
      end
    end
  end

  context "with invalid input" do
    describe "bib record validation failures" do
      let(:response_body) { File.open("spec/fixtures/bib_record_validation_failures.xml") }

      before do
        stub_request(:get, request_url).with(headers: headers).to_return(status: 200, body: response_body, headers: {})
        stub_request(:get, api_uri).with(body: {"{}" => nil}, headers: api_headers)
        subject.extract_bib_record
      end

      it "logs all validation errors" do
        expect(subject.flattened_errors.count).to be(3)
      end

      it "includes identifiable information about each validation error" do
        expect(subject.flattened_errors).to include(match(/mms_id is not present/))
        expect(subject.flattened_errors).to include(match(/title is not present/))
        expect(subject.flattened_errors).to_not include(match(/author is not present/))
        expect(subject.flattened_errors).to include(match(/suppress_from_publishing is not present/))
      end
    end

    describe "portfolio validation failures" do
      let(:response_body) { File.open("spec/fixtures/portfolio_validation_failures.xml") }
      let(:request_url) { "#{api_uri}/almaws/v1/electronic/e-collections/61249556820006535/e-services/62249556810006535/portfolios/#{portfolio_id}?apikey=#{api_key}&format=xml" }

      before do
        stub_request(:get, request_url).with(headers: headers).to_return(status: 200, body: response_body, headers: {})
        stub_request(:get, api_uri).with(body: {"{}" => nil}, headers: api_headers)
        subject.extract_portfolio
      end

      it "logs all validation errors" do
        expect(subject.flattened_errors.count).to be(5)
      end

      it "includes identifiable information about each validation error" do
        expect(subject.flattened_errors).to include(match(/mms_id is not present/))
        expect(subject.flattened_errors).to include(match(/static_url is not present/))
        expect(subject.flattened_errors).to include(match(/portfolio id is not present/))
        expect(subject.flattened_errors).to include(match(/available_from_date is not present/))
        expect(subject.flattened_errors).to include(match(/available_until_date is not present/))
      end
    end
  end

  context "when the Bib record api doesn't return status code 200" do
    describe "returns status code 400 - unauthorized access" do
      let(:response_body) { File.open("spec/fixtures/unauthorized_status_400.xml") }

      before do
        stub_request(:get, request_url).with(headers: headers).to_return(status: 400, body: response_body, headers: {})
        stub_request(:get, api_uri).with(body: {"{}" => nil}, headers: api_headers)
        subject.extract_bib_record
      end

      it "logs all validation errors" do
        expect(subject.flattened_errors.count).to be(1)
      end

      it "includes error code and error message" do
        expect(subject.flattened_errors).to include(match(/UNAUTHORIZED/))
        expect(subject.flattened_errors).to include(match(/API-key not defined or not configured to allow this API/))
      end
    end

    describe "returns status code 400 for invalid mmsId" do
      let(:response_body) { File.open("spec/fixtures/bad_request_status_400.xml") }

      before do
        stub_request(:get, request_url).with(headers: headers).to_return(status: 400, body: response_body, headers: {})
        stub_request(:get, api_uri).with(body: {"{}" => nil}, headers: api_headers)
        subject.extract_bib_record
      end

      it "logs all validation errors" do
        expect(subject.flattened_errors.count).to be(1)
      end

      it "includes error code and error message" do
        expect(subject.flattened_errors).to include(match(/402203/))
        expect(subject.flattened_errors).to include(match(/Input parameters mmsId 111111111111 is not valid./))
      end
    end

    describe "returns status code 400 for missing mmsId" do
      let(:response_body) { File.open("spec/fixtures/missing_mmsid_status_400.xml") }

      before do
        stub_request(:get, request_url).with(headers: headers).to_return(status: 400, body: response_body, headers: {})
        stub_request(:get, api_uri).with(body: {"{}" => nil}, headers: api_headers)
        subject.extract_bib_record
      end

      it "logs all validation errors" do
        expect(subject.flattened_errors.count).to be(1)
      end

      it "includes error code and error message" do
        expect(subject.flattened_errors).to include(match(/401873/))
        expect(subject.flattened_errors).to include(match(/Request failed: Parameter missing. Try one of the following: mms_id, holdings_id, ie_id, representation_id, nz_mms_id, cz_mms_id, other_system_id./))
      end
    end
  end

  context "when the Portfolio api doesn't return status code 200" do
    let(:request_url) { "#{api_uri}/almaws/v1/electronic/e-collections/61249556820006535/e-services/62249556810006535/portfolios/#{portfolio_id}?apikey=#{api_key}&format=xml" }

    describe "returns status code 500 for lengthy portfolio id" do
      let(:response_body) { File.open("spec/fixtures/portfolio_status_500.xml") }

      before do
        stub_request(:get, request_url).with(headers: headers).to_return(status: 500, body: response_body, headers: {})
        stub_request(:get, api_uri).with(body: {"{}" => nil}, headers: api_headers)
        subject.extract_portfolio
      end

      it "logs all validation errors" do
        expect(subject.flattened_errors.count).to be(1)
      end

      it "includes error code and error message" do
        expect(subject.flattened_errors).to include(match(/INTERNAL_SERVER_ERROR/))
        expect(subject.flattened_errors).to include(match(/The web server encountered an unexpected condition that prevented it from fulfilling the request. If the error persists, please use the unique tracking ID when reporting it./))
      end
    end

    describe "returns status code 400 for unauthorized access" do
      let(:response_body) { File.open("spec/fixtures/portfolio_status_400_unauthorized.xml") }

      before do
        stub_request(:get, request_url).with(headers: headers).to_return(status: 400, body: response_body, headers: {})
        stub_request(:get, api_uri).with(body: {"{}" => nil}, headers: api_headers)
        subject.extract_portfolio
      end

      it "logs all validation errors" do
        expect(subject.flattened_errors.count).to be(1)
      end

      it "includes error code and error message" do
        expect(subject.flattened_errors).to include(match(/UNAUTHORIZED/))
        expect(subject.flattened_errors).to include(match(/API-key not defined or not configured to allow this API./))
      end
    end

    describe "returns status code 400 for invalid portfolio id" do
      let(:response_body) { File.open("spec/fixtures/portfolio_status_400.xml") }

      before do
        stub_request(:get, request_url).with(headers: headers).to_return(status: 400, body: response_body, headers: {})
        stub_request(:get, api_uri).with(body: {"{}" => nil}, headers: api_headers)
        subject.extract_portfolio
      end

      it "logs all validation errors" do
        expect(subject.flattened_errors.count).to be(1)
      end

      it "includes error code and error message" do
        expect(subject.flattened_errors).to include(match(/60123/))
        expect(subject.flattened_errors).to include(match(/The portfolio ID is not valid: 53251108420006533/))
      end
    end

    describe "returns status code 400 for invalid service id for list of portfolio" do
      let(:response_body) { File.open("spec/fixtures/portfolios_status_400.xml") }

      before do
        stub_request(:get, request_url).with(headers: headers).to_return(status: 400, body: response_body, headers: {})
        stub_request(:get, api_uri).with(body: {"{}" => nil}, headers: api_headers)
        subject.extract_portfolio
      end

      it "logs all validation errors" do
        expect(subject.flattened_errors.count).to be(1)
      end

      it "includes error code and error message" do
        expect(subject.flattened_errors).to include(match(/60122/))
        expect(subject.flattened_errors).to include(match(/he service ID is not valid: 62182133180006539999/))
      end
    end
  end

  describe "#lookup_portfolio_id" do
    let(:response_body) { File.open("spec/fixtures/sample25Portfolios.xml") }
    let(:request_url) { "#{api_uri}/almaws/v1/electronic/e-collections/61249556820006535/e-services/62249556810006535/portfolios?apikey=#{api_key}&limit=100&offset=0&format=xml" }

    before do
      stub_request(:get, request_url).with(headers: headers).to_return(status: 200, body: response_body, headers: {})
      stub_request(:get, api_uri).with(body: {"{}" => nil}, headers: api_headers)
    end

    it "returns correct portfolioId" do
      expect(subject.lookup_portfolio_id).to eq("53272118260006535")
    end
  end

  describe "record title with any whitespace or slashes at the end" do
    let(:response_body) { File.open("spec/fixtures/sampleAlmaRecordTitle.xml") }

    before do
      stub_request(:get, request_url).with(headers: headers).to_return(status: 200, body: response_body, headers: {})
      stub_request(:get, api_uri).with(body: {"{}" => nil}, headers: api_headers)
      subject.extract_bib_record
    end

    it "drops ending whitespace or slashes" do
      expect(subject.records.first.title).to eq("Allo performance!")
    end
  end

  describe "portfolio with perpetual license type with no available_until_date" do
    let(:response_body) { File.open("spec/fixtures/sampleAlmaSinglePortfolioPerpetualXml.xml") }
    let(:request_url) { "#{api_uri}/almaws/v1/electronic/e-collections/61249556820006535/e-services/62249556810006535/portfolios/#{portfolio_id}?apikey=#{api_key}&format=xml" }

    before do
      stub_request(:get, request_url).with(headers: headers).to_return(status: 200, body: response_body, headers: {})
      stub_request(:get, api_uri).with(body: {"{}" => nil}, headers: api_headers)
      subject.extract_portfolio
    end

    it "has no validation errors" do
      expect(subject.flattened_errors).to_not include(match(/available_until_date is not present/))
      expect(subject.flattened_errors.count).to be(0)
    end
  end
end
