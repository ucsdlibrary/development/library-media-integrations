require "spec_helper"

RSpec.describe App::Email do
  include Mail::Matchers

  context "send success email" do
    before do
      Mail::TestMailer.deliveries.clear
      described_class.send(
        subject: "email subject",
        description: "email description",
        mms_id: "134234234234234234",
        file_name: "somereallyneatfile-134234234234234234.mp4"
      )
    end

    describe "sends email" do
      it { is_expected.to have_sent_email }
    end
  end

  context "send success email with additional fields" do
    before do
      Mail::TestMailer.deliveries.clear
      described_class.send(
        subject: "email subject",
        description: "email description",
        warning: "email warning",
        other_thing: "random thing",
        mms_id: "123123",
        file_name: "filename-123.mp4"
      )
    end

    describe "sends email" do
      it "sends an email with additional fields present" do
        expect(Mail::TestMailer.deliveries.count).to eq(1)

        email = Mail::TestMailer.deliveries.first

        # Checking the body content
        expect(email.body.decoded).to include("Description: email description")
        expect(email.body.decoded).to include("Warning: email warning")
        expect(email.body.decoded).to include("Other Thing: random thing")
        expect(email.body.decoded).to include("MMS ID: 123123")
        expect(email.body.decoded).to include("Filename: filename-123.mp4")
      end
    end
  end

  context "send error email" do
    before do
      Mail::TestMailer.deliveries.clear
      described_class.send(
        subject: "email subject",
        description: "email description",
        mms_id: "134234234234234234",
        file_name: "somereallyneatfile-134234234234234234.mp4",
        errors: ["error1", "error2", "error3"]
      )
    end

    describe "sends email" do
      it { is_expected.to have_sent_email }
    end
  end

  context "send email to multiple recipients" do
    before do
      Mail::TestMailer.deliveries.clear
      described_class.send(
        subject: "email subject",
        description: "email description",
        mms_id: "987654321",
        file_name: "multirecipient-file.mp4",
        to: [ENV.fetch("EMAIL_TO"), ENV.fetch("EMAIL_STATUS_CHECK")]
      )
    end

    describe "sends email" do
      it "sends email to multiple recipients" do
        expect(Mail::TestMailer.deliveries.count).to eq(1)

        email = Mail::TestMailer.deliveries.first

        # Checking the recipients
        expect(email.to).to match_array([ENV.fetch("EMAIL_TO"), ENV.fetch("EMAIL_STATUS_CHECK")])

        # Checking the body content
        expect(email.body.decoded).to include("Description: email description")
        expect(email.body.decoded).to include("MMS ID: 987654321")
        expect(email.body.decoded).to include("Filename: multirecipient-file.mp4")
      end
    end
  end
end
