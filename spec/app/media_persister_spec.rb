require "spec_helper"

RSpec.describe App::MediaPersister do
  let(:api_key) { ENV.fetch("ALMA_API_KEY") }
  let(:api_uri) { ENV.fetch("ALMA_API_URL") }
  let(:response_body) { File.open("spec/fixtures/sampleAlmaBibRecordXml.xml") }
  let(:headers) do
    {
      "Accept" => "*/*",
      "Accept-Encoding" => "gzip;q=1.0,deflate;q=0.6,identity;q=0.3",
      "Host" => "api-example.com",
      "User-Agent" => "Ruby"

    }
  end
  let(:api_headers) do
    {
      "Authorization" => "Bearer 1234",
      "Content-Type" => "application/xml"
    }
  end
  let(:file_name) { "sample-991020477189706535.mp4" }
  let(:portfolio_id) { "53272118260006535" }
  let(:request_url) { "#{api_uri}/almaws/v1/bibs/991020477189706535?apikey=#{api_key}&format=xml" }
  let(:transform_object) { App::Transform.new(file_name: file_name) }

  before do
    stub_const("ENV", ENV.to_hash.merge("ALMA_API_URL" => "http://api-example.com"))
    stub_const("ENV", ENV.to_hash.merge("ALMA_API_KEY" => "some-key"))
    allow(transform_object).to receive(:lookup_portfolio_id).and_return(portfolio_id)
    stub_request(:get, request_url).with(headers: headers).to_return(status: 200, body: response_body, headers: {})
    stub_request(:get, api_uri).with(body: {"{}" => nil}, headers: api_headers)
    transform_object.extract_bib_record
    response_body = File.open("spec/fixtures/sampleAlmaSinglePortfolioCurrentLicense.xml")
    request_url = "#{api_uri}/almaws/v1/electronic/e-collections/61249556820006535/e-services/62249556810006535/portfolios/#{portfolio_id}?apikey=#{api_key}&format=xml"
    stub_request(:get, request_url).with(headers: headers).to_return(status: 200, body: response_body, headers: {})
    stub_request(:get, api_uri).with(body: {"{}" => nil}, headers: api_headers)
    transform_object.extract_portfolio
  end

  context "persisting data" do
    it "persists data for Kaltura upload" do
      persister = described_class.new(records: transform_object.records, portfolios: transform_object.portfolios, file_name: "mms_id1234-1.mp4")
      expect { persister.persist }.to change { MediaResource.count }.from(0).to(1)
      data = persister.persist
      expect(data.name).to eq("Blueness")
      expect(data.original_filename).to eq("mms_id1234-1.mp4")
      expect(data.reference_id).to eq("991020477189706535")
      expect(data.creator_id).to eq("Schroedinger, Kerstin")
      expect(data.start_date).to eq(Date.parse("2024-03-01Z"))
      expect(data.end_date).to eq(Date.parse("2030-05-31Z"))
      expect(data.data_url).to eq("jkey=https://mediaspace.ucsd.edu/media/Blueness_AD/1_pfdy9n9b")
      expect(data.license_type).to eq("current")
    end

    it "throws an error when persistence fails" do
      bib_record_missing_title = [BibRecord.new(mms_id: "1234", author: "author")]
      portfolio_with_everything = [Portfolio.new(mms_id: "1234", available_from_date: Date.today, available_until_date: Date.today, access_type: "current", static_url: "the-url")]

      persister = described_class.new(records: bib_record_missing_title, portfolios: portfolio_with_everything, file_name: "mms_id1234-1.mp4")
      expect { persister.persist }.to raise_error(Sequel::NotNullConstraintViolation)
    end
  end
end
