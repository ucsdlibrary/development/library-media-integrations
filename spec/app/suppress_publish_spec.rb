require "spec_helper"

RSpec.describe App::SuppressPublish do
  let(:api_key) { ENV.fetch("ALMA_API_KEY") }
  let(:api_uri) { ENV.fetch("ALMA_API_URL") }
  let(:response_body) { File.open("spec/fixtures/sampleAlmaBibRecordXml.xml") }
  let(:headers) do
    {
      "Accept" => "*/*",
      "Accept-Encoding" => "gzip;q=1.0,deflate;q=0.6,identity;q=0.3",
      "Host" => "api-example.com",
      "User-Agent" => "Ruby"

    }
  end
  let(:api_headers) do
    {
      "Authorization" => "Bearer 1234",
      "Content-Type" => "application/xml"
    }
  end
  let!(:expired_media_record) do
    MediaResource.create(
      name: "name",
      original_filename: "a-video-mms_id1234-1.mp4",
      creator_id: "creator_id-1",
      reference_id: "991020477189706535",
      data_url: "http://data-url.com",
      license_type: "current",
      start_date: Date.new(2024, 2, 3),
      end_date: Date.new(2024, 3, 3),
      expired: true
    ).save
  end
  let(:request_url) { "#{api_uri}/almaws/v1/bibs/991020477189706535?apikey=#{api_key}&format=xml" }
  let(:subject) { described_class.new(media_record: expired_media_record) }

  before do
    stub_const("ENV", ENV.to_hash.merge("ALMA_API_URL" => "http://api-example.com"))
    stub_const("ENV", ENV.to_hash.merge("ALMA_API_KEY" => "some-key"))
  end

  context "with valid mmsid" do
    describe "#publish" do
      before do
        stub_request(:get, request_url).with(headers: headers).to_return(status: 200, body: response_body, headers: {})
        stub_request(:get, api_uri).with(body: {"{}" => nil}, headers: api_headers)
        response_body = File.open("spec/fixtures/updateAlmaSuppressField.xml")
        stub_request(:get, request_url).with(headers: headers).to_return(status: 200, body: response_body, headers: {})
        stub_request(:get, api_uri).with(body: {"{}" => nil}, headers: api_headers)
      end

      it "change the suppress_publishing field to true" do
        expect(subject.suppress).to include("<suppress_from_publishing>true</suppress_from_publishing>")
      end
    end
  end

  context "with invalid mmsid" do
    describe "#publish" do
      before do
        stub_request(:get, request_url).with(headers: headers).to_return(status: 200, body: response_body, headers: {})
        stub_request(:get, api_uri).with(body: {"{}" => nil}, headers: api_headers)
        response_body = File.open("spec/fixtures/bad_request_update_bib.xml")
        stub_request(:get, request_url).with(headers: headers).to_return(status: 400, body: response_body, headers: {})
        stub_request(:get, api_uri).with(body: {"{}" => nil}, headers: api_headers)
        subject.suppress
      end

      it "returns error code and error message" do
        expect(subject.errors.flatten).to include(match(/<errorCode>402203/))
        expect(subject.errors.flatten).to include(match(/<errorMessage>Input parameters mmsId 9910204771897065351111 is not valid./))
      end
    end
  end

  context "with valid mmsid" do
    describe "#unsuppress" do
      before do
        response_body = File.open("spec/fixtures/updateAlmaSuppressField.xml")
        stub_request(:get, request_url).with(headers: headers).to_return(status: 200, body: response_body, headers: {})
        stub_request(:get, api_uri).with(body: {"{}" => nil}, headers: api_headers)
        response_body = File.open("spec/fixtures/sampleAlmaBibRecordXml.xml")
        stub_request(:get, request_url).with(headers: headers).to_return(status: 200, body: response_body, headers: {})
        stub_request(:get, api_uri).with(body: {"{}" => nil}, headers: api_headers)
      end

      it "change the suppress_publishing field to false" do
        expect(subject.unsuppress).to include("<suppress_from_publishing>false</suppress_from_publishing>")
      end
    end
  end
end
