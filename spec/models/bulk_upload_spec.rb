require "spec_helper"

RSpec.describe BulkUpload do
  describe "#complete_and_save" do
    let!(:bulk_upload_in_progress) do
      BulkUpload.create(
        bulk_job_id: "29105704152",
        status: :in_progress
      )
    end
    it "marks the BulkUpload as :complete and saves it" do
      expect(described_class.dataset.in_progress.map(&:bulk_job_id)).to contain_exactly(bulk_upload_in_progress.bulk_job_id)
      bulk_upload_in_progress.complete_and_save
      expect(described_class.dataset.complete.map(&:bulk_job_id)).to contain_exactly(bulk_upload_in_progress.bulk_job_id)
      expect(described_class.dataset.in_progress.map(&:bulk_job_id)).to contain_exactly
      expect(bulk_upload_in_progress.status).to be(:complete)
    end
  end

  describe "status enum" do
    let!(:bulk_upload_in_progress1) do
      BulkUpload.create(
        bulk_job_id: "29105704152",
        status: :in_progress
      )
    end
    let!(:bulk_upload_in_progress2) do
      BulkUpload.create(
        bulk_job_id: "29105704153",
        status: :in_progress
      )
    end
    let!(:bulk_upload_complete) do
      BulkUpload.create(
        bulk_job_id: "29105704154",
        status: :complete
      )
    end

    it "provides dataset methods to query based on enum value" do
      expect(described_class.dataset.in_progress.map(&:bulk_job_id)).to contain_exactly(bulk_upload_in_progress1.bulk_job_id, bulk_upload_in_progress2.bulk_job_id)

      expect(described_class.dataset.complete.map(&:bulk_job_id)).to contain_exactly(bulk_upload_complete.bulk_job_id)
    end
  end

  describe "MediaResource association" do
    context "with an associated MediaResource entry" do
      let(:media_record) do
        MediaResource.create(
          name: "name",
          original_filename: "a-video-mms_id1234-1.mp4",
          creator_id: "creator_id-1",
          reference_id: "mms_id1234-1",
          data_url: "http://data-url.com",
          license_type: "perpetual",
          start_date: Date.new(2024, 2, 3)
        )
      end

      let(:bulk_upload) do
        BulkUpload.create(
          bulk_job_id: "29105704152",
          status: :in_progress
        )
      end

      before do
        bulk_upload.media_resource = media_record
      end

      it "can retrieve the record using an association" do
        expect(bulk_upload.media_resource.reference_id).to eq("mms_id1234-1")
      end
    end
    context "supports a MediaResource belonging to multiple BulkUpload entries" do
      let(:media_record) do
        MediaResource.create(
          name: "name",
          original_filename: "a-video-mms_id1234-1.mp4",
          creator_id: "creator_id-1",
          reference_id: "mms_id1234-1",
          data_url: "http://data-url.com",
          license_type: "perpetual",
          start_date: Date.new(2024, 2, 3)
        )
      end

      let(:bulk_upload_in_progress) do
        BulkUpload.create(
          bulk_job_id: "29105704152",
          status: :in_progress
        )
      end
      let(:bulk_upload_failed) do
        BulkUpload.create(
          bulk_job_id: "29105704153",
          status: :complete
        )
      end

      before do
        bulk_upload_in_progress.media_resource = media_record
        bulk_upload_failed.media_resource = media_record
      end

      it "can retrieve the record using an association" do
        expect(bulk_upload_in_progress.media_resource.reference_id).to eq("mms_id1234-1")
        expect(bulk_upload_failed.media_resource.reference_id).to eq("mms_id1234-1")
      end
    end
    context "without an associated MediaResource entry" do
      let(:bulk_upload) do
        BulkUpload.create(
          bulk_job_id: "29105704152",
          status: :in_progress
        )
      end

      it "cannot retrieve the record with an association" do
        expect(bulk_upload.media_resource).to be nil
      end
    end
  end
end
