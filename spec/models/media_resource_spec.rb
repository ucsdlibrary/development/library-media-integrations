require "spec_helper"

RSpec.describe MediaResource do
  describe "validate" do
    context "without an end_date" do
      let(:media_record) do
        MediaResource.new(
          name: "name",
          original_filename: "a-video-mms_id1234-1.mp4",
          creator_id: "creator_id-1",
          reference_id: "mms_id1234-1",
          data_url: "http://data-url.com",
          license_type: "perpetual",
          start_date: Date.new(2024, 2, 3)
        )
      end

      it "start_date is valid" do
        expect(media_record.valid?).to be true
      end
    end

    context "with an end_date" do
      let(:invalid_media_record) do
        MediaResource.new(
          name: "name",
          original_filename: "a-video-mms_id1234-1.mp4",
          creator_id: "creator_id-1",
          reference_id: "mms_id1234-1",
          data_url: "http://data-url.com",
          license_type: "perpetual",
          start_date: Date.new(2028, 2, 3),
          end_date: Date.new(2024, 2, 3)
        )
      end
      let(:valid_media_record) do
        MediaResource.new(
          name: "name",
          original_filename: "a-video-mms_id1234-1.mp4",
          creator_id: "creator_id-1",
          reference_id: "mms_id1234-1",
          data_url: "http://data-url.com",
          license_type: "current",
          start_date: Date.new(2024, 2, 3),
          end_date: Date.new(2028, 2, 3)
        )
      end

      it "a start_date older than an end_date is invalid" do
        expect(invalid_media_record.valid?).to be false
        expect(invalid_media_record.errors).to be { :start_date => ["cannot be after end_date"] }
      end
      it "a start_date before an end_date is valid" do
        expect(valid_media_record.valid?).to be true
      end
    end

    context "with current license_type" do
      let(:valid_media_record) do
        MediaResource.new(
          name: "name",
          original_filename: "a-video-mms_id1234-1.mp4",
          creator_id: "creator_id-1",
          reference_id: "mms_id1234-1",
          data_url: "http://data-url.com",
          license_type: "current",
          start_date: Date.new(2024, 2, 3),
          end_date: Date.new(2028, 2, 3)
        )
      end
      let(:invalid_media_record) do
        MediaResource.new(
          name: "name",
          original_filename: "a-video-mms_id1234-1.mp4",
          creator_id: "creator_id-1",
          reference_id: "mms_id1234-1",
          data_url: "http://data-url.com",
          license_type: "current",
          start_date: Date.new(2024, 2, 3)
        )
      end

      it "current license type with end_date is valid" do
        expect(valid_media_record.valid?).to be true
      end
      it "current license type with no end_date is invalid" do
        expect(invalid_media_record.valid?).to be false
        expect(invalid_media_record.errors[:license_type]).to include("end_date is required for current license type")
      end
    end

    context "with perpetual license_type" do
      let(:valid_media_record) do
        MediaResource.new(
          name: "name",
          original_filename: "a-video-mms_id1234-1.mp4",
          creator_id: "creator_id-1",
          reference_id: "mms_id1234-1",
          data_url: "http://data-url.com",
          license_type: "perpetual",
          start_date: Date.new(2024, 2, 3)
        )
      end
      let(:invalid_media_record) do
        MediaResource.new(
          name: "name",
          original_filename: "a-video-mms_id1234-1.mp4",
          creator_id: "creator_id-1",
          reference_id: "mms_id1234-1",
          data_url: "http://data-url.com",
          license_type: "perpetual",
          start_date: Date.new(2024, 2, 3),
          end_date: Date.new(2028, 2, 3)
        )
      end

      it "perpetual license type with no end_date is valid" do
        expect(valid_media_record.valid?).to be true
      end
      it "perpetual license type with end_date is invalid" do
        expect(invalid_media_record.valid?).to be false
        expect(invalid_media_record.errors[:license_type]).to include("cannot have end_date for perpetual license type")
      end
    end

    context "with license_type not current or perpetual" do
      let(:invalid_media_record) do
        MediaResource.new(
          name: "name",
          original_filename: "a-video-mms_id1234-1.mp4",
          creator_id: "creator_id-1",
          reference_id: "mms_id1234-1",
          data_url: "http://data-url.com",
          license_type: "other_type",
          start_date: Date.new(2024, 2, 3)
        )
      end

      it "invalid_media_record is invalid" do
        expect(invalid_media_record.valid?).to be false
        expect(invalid_media_record.errors[:license_type]).to include("can only be current or perpetual license type")
      end
    end
  end
  describe "to_kaltura_upload" do
    let!(:media_record) do
      MediaResource.create(
        name: "name",
        original_filename: "a-video-mms_id1234-1.mp4",
        creator_id: "creator_id-1",
        reference_id: "mms_id1234-1",
        data_url: "http://data-url.com",
        license_type: "current",
        start_date: Date.new(2024, 2, 3),
        end_date: Date.new(2028, 2, 3)
      ).save
    end
    it "serializes metadata to a hash for kaltura upload" do
      expect(media_record.to_kaltura_upload.keys).to contain_exactly(:data_url, :start_date, :end_date, :creator_id, :reference_id, :name, :license_type)
    end
    it "formats license dates for kaltura upload" do
      expect(media_record.to_kaltura_upload[:start_date]).to eq("2024-02-03+00:00")
      expect(media_record.to_kaltura_upload[:end_date]).to eq("2028-02-03+00:00")
    end
    it "defaults to not being expired" do
      expect(media_record[:expired]).to be false
    end
  end
  describe "to_kaltura_upload without end_date" do
    let!(:media_record_no_end_date) do
      MediaResource.create(
        name: "name",
        original_filename: "a-video-mms_id1234-1.mp4",
        creator_id: "creator_id-1",
        reference_id: "mms_id1234-1",
        data_url: "http://data-url.com",
        license_type: "perpetual",
        start_date: Date.new(2024, 2, 3)
      ).save
    end
    it "returns nil value for end_date" do
      expect(media_record_no_end_date.to_kaltura_upload[:end_date]).to be_nil
    end
  end
  describe ".resources_to_expire" do
    let!(:active_media_record) do
      MediaResource.create(
        name: "name",
        original_filename: "a-video-mms_id1234-1.mp4",
        creator_id: "creator_id-1",
        reference_id: "mms_id1234-1",
        data_url: "http://data-url.com",
        license_type: "current",
        start_date: Date.new(2024, 2, 3),
        end_date: Date.new(2028, 2, 3)
      ).save
    end
    let!(:media_record_to_expire) do
      MediaResource.create(
        name: "record-to-expire",
        original_filename: "a-video-mms_id1234-1.mp4",
        creator_id: "creator_id-1",
        reference_id: "mms_id1234-1",
        data_url: "http://data-url.com",
        license_type: "current",
        start_date: Date.new(2022, 2, 3),
        end_date: Date.new(2023, 2, 3)
      ).save
    end
    let!(:expired_media_record) do
      MediaResource.create(
        name: "name",
        original_filename: "a-video-mms_id1234-1.mp4",
        creator_id: "creator_id-1",
        reference_id: "mms_id1234-1",
        data_url: "http://data-url.com",
        license_type: "current",
        start_date: Date.new(2024, 2, 3),
        end_date: Date.new(2028, 2, 3),
        expired: true
      ).save
    end
    it "does not include already expired or active records" do
      expect(described_class.resources_to_expire.count).to be 1
      expect(described_class.resources_to_expire.first.name).to eq media_record_to_expire.name
    end
  end
end
