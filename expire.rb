require "opentelemetry/sdk"
require "opentelemetry/instrumentation/all"
require "opentelemetry-exporter-otlp"
require_relative "app"

# don't buffer log output
$stdout.sync = true

# OpenTelemetry configuration
# see: https://opentelemetry.io/docs/languages/ruby/getting-started/#instrumentation
OpenTelemetry::SDK.configure do |c|
  c.logger = App::Logger.logger
  c.service_name = "library-media-integrations"
  c.use_all # enables all instrumentation!
end

# setup tracer for the application
# see: https://opentelemetry.io/docs/languages/ruby/instrumentation/#traces
tracer_provider = OpenTelemetry.tracer_provider
LibraryMediaTracer = tracer_provider.tracer("LibraryMediaIntegrationsTracer")

LibraryMediaTracer.in_span("Expiration Transaction") do |span|
  span.add_event("Initiating expiration transaction")
  MediaResource.resources_to_expire.each do |media_record|
    App::Transactions::Expire.call(media_record) do |result|
      result.success do
        msg = "Successfully expired #{media_record.reference_id}:  [#{media_resource.name}]"
        App::Logger.logger.info(msg)
        App::Email.send(subject: msg,
          description: "Streaming license for #{media_record.name} has expired. It has been suppressed from Primo and files have been deleted from Kaltura and local storage",
          mms_id: media_record.reference_id,
          kaltura_entry_id: media_record.entry_id,
          file_name: media_record.original_filename)
      end
      result.failure do |failure|
        streaming_link = media_record.data_url.include?("jkey=") ? media_record.data_url.gsub!("jkey=", "") : ""
        msg = "Failed to expire #{media_record.reference_id}"
        App::Logger.logger.error(msg)
        App::Logger.logger.error(failure)
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(failure) if failure.is_a? Exception
        App::Email.send(subject: msg,
          description: "Failure to process expired streaming license for #{media_record.name}",
          mms_id: media_record.reference_id,
          kaltura_entry_id: media_record.entry_id,
          streaming_link: streaming_link,
          file_name: media_record.original_filename,
          errors: failure)
      end
    end
  end
end

# shutdown/close the tracer_provider
tracer_provider.shutdown
