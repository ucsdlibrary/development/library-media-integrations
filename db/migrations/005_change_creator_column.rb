Sequel.migration do
  change do
    alter_table :media_resources do
      set_column_allow_null :creator_id
    end
  end
end
