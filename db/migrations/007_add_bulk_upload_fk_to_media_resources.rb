Sequel.migration do
  change do
    alter_table :media_resources do
      add_foreign_key :bulk_upload_id, :bulk_uploads
    end
  end
end
