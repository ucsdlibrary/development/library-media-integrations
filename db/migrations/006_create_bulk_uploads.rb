Sequel.migration do
  up do
    create_table :bulk_uploads do
      primary_key :id
      foreign_key :media_resource_id, :media_resources
      Bignum :bulk_job_id, null: false, index: true
      Integer :status, null: false, index: true
    end
  end

  down do
    drop_table :bulk_uploads
  end
end
