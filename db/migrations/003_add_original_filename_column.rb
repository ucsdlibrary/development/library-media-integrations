Sequel.migration do
  change do
    alter_table :media_resources do
      add_column :original_filename, String, null: false
    end
  end
end
