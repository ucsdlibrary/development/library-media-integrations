Sequel.migration do
  up do
    create_table :media_resources do
      primary_key :id
      String :name, null: false
      String :creator_id, null: false
      String :reference_id, null: false, index: true # mms_id
      String :data_url, null: false
      String :license_type, null: false
      Date :start_date, null: false, index: true
      Date :end_date, index: true
    end
  end

  down do
    drop_table :media_resources
  end
end
