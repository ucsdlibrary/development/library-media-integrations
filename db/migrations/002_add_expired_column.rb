Sequel.migration do
  change do
    alter_table :media_resources do
      add_column :expired, TrueClass, default: false
    end
  end
end
