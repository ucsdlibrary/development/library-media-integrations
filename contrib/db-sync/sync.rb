require_relative "../../app"

# Give us a hash of key == mmsid and value == original_file
media_files_data = CSV.read("media_files.csv", headers: %i[original_file mmsid]).each_with_object({}) { |row, h| h[row[:mmsid]] = row[:original_file] }
errors = []

media_records_in_kaltura = App::KalturaUtils::CheckFile.check_all_kaltura_files
App::Logger.logger.info("Found #{media_records_in_kaltura.size} UCSD Library Files in Kaltura")
if media_records_in_kaltura.size > 0
  media_records_in_kaltura.each_with_index do |media_entry, index|
    if media_entry.reference_id
      original_file = media_files_data[media_entry.reference_id]
      App::Logger.logger.info("Processing: #{index}- entry_id #{media_entry.id} - portfolio for mmsid #{media_entry.reference_id} - original_file: #{original_file}")
      transformer = App::Transform.new(file_name: original_file)
      transformer.extract_bib_record
      transformer.extract_portfolio
      media_record = App::MediaPersister.new(records: transformer.records, portfolios: transformer.portfolios, file_name: original_file).persist
      App::KalturaUtils::PersistEntryId.call(media_record: media_record, entry_id: media_entry.id)
      App::Logger.logger.info("Finished Processing: #{index}- entry_id #{media_entry.id} - portfolio for mmsid #{media_entry.reference_id} - original_file: #{original_file}")
    else
      App::Logger.logger.info("Failed to process: #{index} - #{media_entry}")
    end
  rescue => e
    App::Logger.logger.error("Failed to process: #{index} - #{media_entry}: #{e.message}")
    errors << media_entry.reference_id
    next
  end
  App::Logger.logger.info("Finish persisting MediaResource records for #{media_records_in_kaltura.size} records")
  App::Logger.logger.info("MediaResource records in database: #{MediaResource.count}")
  print(errors)
else
  msg = "No media entries found"
  App::Logger.logger.error(msg)
end
