# Local Database Sync

This script was needed to support [initial population of the production database][db-gitlab-issue]. In phase 1 of the project we did,
essentially, a manual load of files into Kaltura. Which meant we did not have the local production database populated
with those records.

This script can be used to populate a local database with files belonging to the UCSD Library category in Kaltura. It
_may_ need paging support added to the `KalturaUtils::CheckFile.check_all_kaltura_files` method in the future as the
amount of content is increased.

```sh
docker-compose exec app /bin/sh
cd contrib/db-sync
ruby sync.rb
```

[db-gitlab-issue]:https://gitlab.com/ucsdlibrary/development/library-media-integrations/-/issues/170
