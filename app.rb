require "zeitwerk"

module App
  # see: https://github.com/fxn/zeitwerk/blob/main/README.md
  loader = Zeitwerk::Loader.new
  loader.push_dir("#{__dir__}/app", namespace: App)
  loader.push_dir("#{__dir__}/models")
  loader.push_dir("#{__dir__}/rules")
  loader.push_dir("#{__dir__}/rules/bib_record")
  loader.push_dir("#{__dir__}/rules/portfolio")
  # loader.log! # uncomment if needed for testing
  loader.setup
  loader.eager_load
end
