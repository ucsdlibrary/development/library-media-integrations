class RecordSuppressFromPublishing < BibRecordRule
  def self.call(record:, xml_doc:)
    mms_id = xml_doc.at_css("mms_id")
    raise RuleException.new("suppress_from_publishing is not present in record #{mms_id}") unless xml_doc.at_css("suppress_from_publishing")

    record.suppress_from_publishing = xml_doc.at_css("suppress_from_publishing").text
  end
end
