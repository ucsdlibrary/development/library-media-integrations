class RecordTitle < BibRecordRule
  def self.call(record:, xml_doc:)
    mms_id = xml_doc.at_css("mms_id")
    raise RuleException.new("title is not present in record #{mms_id}") unless xml_doc.xpath("//record/datafield[@tag='245']/subfield[@code='a']").first
    title = xml_doc.xpath("//record/datafield[@tag='245']/subfield[@code='a']").first.content
    record.title = title.delete_suffix("/").strip
  end
end
