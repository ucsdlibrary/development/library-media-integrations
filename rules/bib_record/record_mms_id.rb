class RecordMmsId < BibRecordRule
  def self.call(record:, xml_doc:)
    mms_id = xml_doc.at_css("mms_id")
    raise RuleException.new("mms_id is not present in record #{mms_id}") unless xml_doc.at_css("mms_id")

    record.mms_id = mms_id.text
  end
end
