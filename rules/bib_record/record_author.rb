class RecordAuthor < BibRecordRule
  def self.call(record:, xml_doc:)
    author = xml_doc.at_css("author") ? xml_doc.at_css("author").text : ""
    record.author = author
  end
end
