class PortfolioAvailableFromDate < PortfolioRule
  def self.call(record:, xml_doc:)
    portfolio_id = xml_doc.at_css("id")
    raise RuleException.new("available_from_date is not present in portfolio #{portfolio_id}") unless xml_doc.at_css("available_from_date")
    from_date = xml_doc.at_css("available_from_date") ? xml_doc.at_css("available_from_date").text : ""
    record.available_from_date = from_date
  end
end
