class PortfolioId < PortfolioRule
  def self.call(record:, xml_doc:)
    portfolio_id = xml_doc.at_css("id")
    raise RuleException.new("portfolio id is not present in portfolio #{portfolio_id}") unless xml_doc.at_css("id")

    record.id = portfolio_id.text
  end
end
