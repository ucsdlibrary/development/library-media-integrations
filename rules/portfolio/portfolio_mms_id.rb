class PortfolioMmsId < PortfolioRule
  def self.call(record:, xml_doc:)
    portfolio_id = xml_doc.at_css("id")
    raise RuleException.new("resource_metadata mms_id is not present in portfolio #{portfolio_id}") unless xml_doc.at_css("resource_metadata mms_id")

    record.mms_id = xml_doc.at_css("resource_metadata mms_id").text
  end
end
