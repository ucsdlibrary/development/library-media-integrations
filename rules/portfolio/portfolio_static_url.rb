class PortfolioStaticUrl < PortfolioRule
  def self.call(record:, xml_doc:)
    portfolio_id = xml_doc.at_css("id")
    raise RuleException.new("linking_details static_url is not present in portfolio #{portfolio_id}") unless xml_doc.at_css("linking_details static_url")

    record.static_url = xml_doc.at_css("linking_details static_url").text
  end
end
