class PortfolioAccessType < PortfolioRule
  def self.call(record:, xml_doc:)
    portfolio_id = xml_doc.at_css("id")
    raise RuleException.new("access_type is not present in portfolio #{portfolio_id}") unless xml_doc.at_css("access_type")

    record.access_type = xml_doc.at_css("access_type").text
  end
end
