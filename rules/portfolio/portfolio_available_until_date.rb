class PortfolioAvailableUntilDate < PortfolioRule
  PERPETUAL_LICENSE_TYPE = "perpetual".freeze
  def self.call(record:, xml_doc:)
    portfolio_id = xml_doc.at_css("id")
    access_type = xml_doc.at_css("access_type").text
    raise RuleException.new("available_until_date is not present in portfolio #{portfolio_id}") unless xml_doc.at_css("available_until_date") || access_type == PERPETUAL_LICENSE_TYPE
    until_date = xml_doc.at_css("available_until_date") ? xml_doc.at_css("available_until_date").text : ""
    record.available_until_date = until_date
  end
end
