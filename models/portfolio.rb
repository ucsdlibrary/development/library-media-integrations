# Data structure representing a single portfolio extracted from an Alma XML export
Portfolio = Struct.new(
  :mms_id, :available_from_date, :available_until_date, :access_type, :static_url, :id
)
