# Sequel requires the database connection be set up before model class definition, since it parses the database schema
# on model class creation.
App::DbConfig.connection

class BulkUpload < Sequel::Model
  many_to_one :media_resource

  def complete_and_save
    self.status = :complete
    save
  end
end

# see: https://sequel.jeremyevans.net/rdoc-plugins/classes/Sequel/Plugins/Enum.html
BulkUpload.plugin :enum
BulkUpload.enum :status, in_progress: 1, complete: 2
