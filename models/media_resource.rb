# Sequel requires the database connection be set up before model class definition, since it parses the database schema
# on model class creation.
App::DbConfig.connection

class MediaResource < Sequel::Model
  one_to_many :bulk_uploads

  EXCLUDE_FROM_KALTURA_UPLOAD = %i[id expired original_filename entry_id bulk_upload_id]
  CURRENT_LICENSE_TYPE = "current".freeze
  PERPETUAL_LICENSE_TYPE = "perpetual".freeze
  # Resources that have an end_date that is either today or in the past AND are not already expired
  def self.resources_to_expire
    where { end_date <= Date.today }.exclude { expired }
  end

  # override
  # @see: https://sequel.jeremyevans.net/rdoc/files/doc/validations_rdoc.html#top
  def validate
    super

    if end_date && (start_date > end_date)
      errors.add(:start_date, "cannot be after end_date")
    end

    if end_date && license_type == PERPETUAL_LICENSE_TYPE
      errors.add(:license_type, "cannot have end_date for perpetual license type")
    end

    if !end_date && license_type == CURRENT_LICENSE_TYPE
      errors.add(:license_type, "end_date is required for current license type")
    end

    if license_type != CURRENT_LICENSE_TYPE && license_type != PERPETUAL_LICENSE_TYPE
      errors.add(:license_type, "can only be current or perpetual license type")
    end
  end

  # Serialization to Hash that Kaltura needs for upload
  # properties: :data_url, :start_date, :end_date, :creator_id, :reference_id, :name, :license_type
  def to_kaltura_upload
    # filter out primary key and expired flag
    kaltura_hash = to_hash.except(*EXCLUDE_FROM_KALTURA_UPLOAD)
    # format dates
    kaltura_hash[:start_date] = kaltura_hash[:start_date].strftime("%F%Z")
    kaltura_hash[:end_date] = kaltura_hash[:end_date].strftime("%F%Z") if !kaltura_hash[:end_date].nil?
    kaltura_hash
  end
end
