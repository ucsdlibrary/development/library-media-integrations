# Data structure representing a single Bib record extracted from an Alma XML export
BibRecord = Struct.new(
  :mms_id, :title, :author, :suppress_from_publishing
)
