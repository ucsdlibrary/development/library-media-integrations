require "opentelemetry/sdk"
require "opentelemetry/instrumentation/all"
require "opentelemetry-exporter-otlp"
require_relative "app"

# don't buffer log output
$stdout.sync = true

# OpenTelemetry configuration
# see: https://opentelemetry.io/docs/languages/ruby/getting-started/#instrumentation
OpenTelemetry::SDK.configure do |c|
  c.logger = App::Logger.logger
  c.service_name = "library-media-integrations"
  c.use_all # enables all instrumentation!
end

# setup tracer for the application
# see: https://opentelemetry.io/docs/languages/ruby/instrumentation/#traces
tracer_provider = OpenTelemetry.tracer_provider
LibraryMediaTracer = tracer_provider.tracer("LibraryMediaIntegrationsTracer")

# Run the scrip
LibraryMediaTracer.in_span("Bulk Upload Transaction") do |span|
  App::Seed.new.seed_nfs if ENV.fetch("APP_ENV") == "development"
  Dir["#{ENV.fetch("MEDIA_FILES_PATH")}/*"].each do |media_file|
    next if File.directory?(media_file)
    media_file = File.basename(media_file)

    # Call the upload method
    App::Transactions::BulkUpload.call(media_file) do |result|
      result.success do |media_resource|
        msg = "Kaltura upload started for #{media_resource.reference_id}:  [#{media_resource.name}]"
        App::Logger.logger.info(msg)
        App::Email.send(subject: msg,
          description: "It has successfully initiated an upload job and is preparing to link to Alma/Primo.",
          mms_id: media_resource.reference_id,
          file_name: media_file,
          to: [ENV.fetch("EMAIL_TO")])
      end
      result.failure do |failure|
        msg = "Failed to initiate Kaltura Bulk Upload for #{media_file}"
        App::Logger.logger.error(msg)
        App::Logger.logger.error(failure)
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(failure) if failure.is_a? Exception
        App::Email.send(subject: msg,
          description: msg,
          mms_id: App::Transform.file_name_to_mms_id(file_name: media_file),
          file_name: media_file,
          errors: failure)
      end
    end
  end
end

# shutdown/close the tracer_provider
tracer_provider.shutdown
