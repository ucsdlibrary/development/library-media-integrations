require "opentelemetry/sdk"
require "opentelemetry/instrumentation/all"
require "opentelemetry-exporter-otlp"
require_relative "app"

# don't buffer log output
$stdout.sync = true

# OpenTelemetry configuration
# see: https://opentelemetry.io/docs/languages/ruby/getting-started/#instrumentation
OpenTelemetry::SDK.configure do |c|
  c.logger = App::Logger.logger
  c.service_name = "library-media-integrations"
  c.use_all # enables all instrumentation!
end

PRIMO_BASE_URL = "https://search-library.ucsd.edu/permalink/01UCS_SDI/ld412s/alma".freeze

# setup tracer for the application
# see: https://opentelemetry.io/docs/languages/ruby/instrumentation/#traces
tracer_provider = OpenTelemetry.tracer_provider
LibraryMediaTracer = tracer_provider.tracer("LibraryMediaIntegrationsTracer")

LibraryMediaTracer.in_span("Kaltura Bulk Upload Status Check Transaction") do |span|
  BulkUpload.dataset.in_progress.each do |bulk_upload|
    App::Transactions::BulkUploadStatusCheck.call(bulk_upload) do |result|
      result.success do |media_resource|
        streaming_link = media_resource.data_url.include?("jkey=") ? media_resource.data_url.gsub!("jkey=", "") : ""
        permalink = "#{PRIMO_BASE_URL}#{media_resource.reference_id}"
        msg = "Kaltura upload completed for #{media_resource.reference_id}:  [#{media_resource.name}]"
        App::Logger.logger.info(msg)
        App::Email.send(subject: msg,
          description: "The streaming URL has been linked to Alma and been published.",
          warning: "This may take 15 minutes or more to reflect in Primo.",
          mms_id: media_resource.reference_id,
          kaltura_entry_id: media_resource.entry_id,
          streaming_link: streaming_link,
          permalink: permalink,
          file_name: media_resource.original_filename,
          to: [ENV.fetch("EMAIL_TO"), ENV.fetch("EMAIL_STATUS_CHECK")])
      end
      result.failure :check_bulk_upload_job_status do |failure|
        if failure[:send_notification]
          media_resource = failure[:media_resource]
          streaming_link = media_resource.data_url.include?("jkey=") ? media_resource.data_url.gsub!("jkey=", "") : ""
          msg = failure[:message]
          App::Logger.logger.error(msg)
          span.status = OpenTelemetry::Trace::Status.error(msg)
          span.record_exception(msg) if msg.is_a? Exception
          App::Email.send(subject: "Kaltura Upload Status Check Job failed for #{media_resource.reference_id}",
            description: msg,
            mms_id: media_resource.reference_id,
            kaltura_entry_id: media_resource.entry_id,
            streaming_link: streaming_link,
            file_name: media_resource.original_filename,
            errors: msg)

        end
      end
      result.failure do |failure|
        msg = "Kaltura Bulk Upload failed to complete"
        errors = failure[:message]
        media_resource = failure[:media_resource]
        App::Logger.logger.error(msg)
        App::Logger.logger.error(errors)
        span.status = OpenTelemetry::Trace::Status.error(msg)
        span.record_exception(errors) if errors.is_a? Exception
        App::Email.send(subject: "Kaltura Bulk Upload Status Check failed to complete for #{media_resource.reference_id}",
          description: msg,
          mms_id: media_resource.reference_id,
          kaltura_entry_id: media_resource.entry_id,
          file_name: media_resource.original_filename,
          errors: errors)
      end
    end
  end
end

# shutdown/close the tracer_provider
tracer_provider.shutdown
